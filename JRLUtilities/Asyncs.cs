﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace JonRLevy.JRLUtilities.Tasks
{
    /// <summary>
    /// Contains static utility methods focused on asynchronous loops.
    /// </summary>
    public class Asyncs
    {
        /// <summary>
        /// A static method for the pattern of performing a get and put operation for each item in a collection, such as
        /// getting an item from a WebAPI and inserting it in a database, from a given key. Performs the put and the next get
        /// simultaneously.
        /// </summary>
        /// <typeparam name="T">The type of items in the collection.</typeparam>
        /// <typeparam name="TGetResult">The type of the result from the Get-function.</typeparam>
        /// <param name="collection">The collection that should be looped through.</param>
        /// <param name="funcGetAsync">The asynchronous function to call for each Get.</param>
        /// <param name="funcPutAsync">The asynchronous function to call for each Put.</param>
        /// <returns>The number of Get/Put-operations performed.</returns>
        public static async Task<int> ForEachGetPutAsync<T, TGetResult>(IEnumerable<T> collection, Func<T, Task<TGetResult>> funcGetAsync, Func<TGetResult, Task> funcPutAsync)
        {
            int count = 0;

            using (IEnumerator<T> enumerator = collection.GetEnumerator())
            {
                bool hasItem = enumerator.MoveNext();
                if (!hasItem)
                    return 0;

                Task<TGetResult> TaskGet = funcGetAsync(enumerator.Current);
                Task TaskPut = null;

                while (hasItem)
                {
                    TGetResult gotten = await TaskGet;

                    if (TaskPut != null)
                    {
                        await TaskPut;
                        count++;
                    }

                    TaskPut = funcPutAsync(gotten);

                    hasItem = enumerator.MoveNext();
                    if (hasItem)
                        TaskGet = funcGetAsync(enumerator.Current);
                }

                if (TaskPut != null)
                {
                    await TaskPut;
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// A static method for the pattern of performing a get and put operation for each item in a collection, such as
        /// getting an item from a WebAPI and inserting it in a database, from a given key. Performs the put and the next get
        /// simultaneously.
        /// </summary>
        /// <typeparam name="T">The type of items in the collection.</typeparam>
        /// <typeparam name="TGetResult">The type of the result from the Get-function.</typeparam>
        /// <typeparam name="TGetContext">The type of a context passed to the Get-function. Might be an API-object, for instance.</typeparam>
        /// <typeparam name="TPutContext">The type of a context passed to the Put-function. Might be a database connection, for instance.</typeparam>
        /// <param name="collection">The collection that should be looped through.</param>
        /// <param name="getContext">A context passed to the Get-function. Might be an API-object, for instance.</param>
        /// <param name="putContext">A context passed to the Put-function. Might be a database connection, for instance.</param>
        /// <param name="funcGetAsync">The asynchronous function to call for each Get.</param>
        /// <param name="funcPutAsync">The asynchronous function to call for each Put.</param>
        /// <returns>The number of Get/Put-operations performed.</returns>
        public static async Task<int> ForEachGetPutAsync<T, TGetResult, TGetContext, TPutContext>(IEnumerable<T> collection, TGetContext getContext, TPutContext putContext, Func<TGetContext, T, Task<TGetResult>> funcGetAsync, Func<TPutContext, TGetResult, Task> funcPutAsync)
        {
            int count = 0;

            using (IEnumerator<T> enumerator = collection.GetEnumerator())
            {
                bool hasItem = enumerator.MoveNext();
                if (!hasItem)
                    return 0;

                Task<TGetResult> TaskGet = funcGetAsync(getContext, enumerator.Current);
                Task TaskPut = null;

                while (hasItem)
                {
                    TGetResult gotten = await TaskGet;

                    if (TaskPut != null)
                    {
                        await TaskPut;
                        count++;
                    }

                    TaskPut = funcPutAsync(putContext, gotten);

                    hasItem = enumerator.MoveNext();
                    if (hasItem)
                        TaskGet = funcGetAsync(getContext, enumerator.Current);
                }

                if (TaskPut != null)
                {
                    await TaskPut;
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// A static method for the pattern of performing a get and put operation for each item in a collection, such as
        /// getting an item from a WebAPI and inserting it in a database, from a given key. Performs the put and the next get
        /// simultaneously. Results from the put operations are returned in a List.
        /// </summary>
        /// <typeparam name="T">The type of items in the collection.</typeparam>
        /// <typeparam name="TGetResult">The type of the result from the Get-function.</typeparam>
        /// <typeparam name="TPutResult">The type of the result from the Put-function</typeparam>
        /// <typeparam name="TGetContext">The type of a context passed to the Get-function. Might be an API-object, for instance.</typeparam>
        /// <typeparam name="TPutContext">The type of a context passed to the Put-function. Might be a database connection, for instance.</typeparam>
        /// <param name="collection">The collection that should be looped through.</param>
        /// <param name="getContext">A context passed to the Get-function. Might be an API-object, for instance.</param>
        /// <param name="putContext">A context passed to the Put-function. Might be a database connection, for instance.</param>
        /// <param name="funcGetAsync">The asynchronous function to call for each Get.</param>
        /// <param name="funcPutAsync">The asynchronous function to call for each Put.</param>
        /// <returns>A List containing the results from the Put operations.</returns>
        public static async Task<List<TPutResult>> ForEachGetPutAsync<T, TGetResult, TPutResult, TGetContext, TPutContext>(IEnumerable<T> collection, TGetContext getContext, TPutContext putContext, Func<TGetContext, T, Task<TGetResult>> funcGetAsync, Func<TPutContext, TGetResult, Task<TPutResult>> funcPutAsync)
        {
            List<TPutResult> outResults = new List<TPutResult>();

            using (IEnumerator<T> enumerator = collection.GetEnumerator())
            {
                bool hasItem = enumerator.MoveNext();
                if (!hasItem)
                    return outResults;

                Task<TGetResult> TaskGet = funcGetAsync(getContext, enumerator.Current);
                Task<TPutResult> TaskPut = null;

                while (hasItem)
                {
                    TGetResult gotten = await TaskGet;

                    if (TaskPut != null)
                        outResults.Add(await TaskPut);

                    TaskPut = funcPutAsync(putContext, gotten);

                    hasItem = enumerator.MoveNext();
                    if (hasItem)
                        TaskGet = funcGetAsync(getContext, enumerator.Current);
                }

                if (TaskPut != null)
                    outResults.Add(await TaskPut);
            }

            return outResults;
        }

        /// <summary>
        /// Basically an asynchronous wrapper for the Parallel.ForEach method.
        /// </summary>
        /// <typeparam name="T">The type of items in the collection</typeparam>
        /// <param name="collection">The collection of items something should be done with.</param>
        /// <param name="action">The action to perform - in parallel - for each item.</param>
        /// <returns>A Task object for the operation.</returns>
        public static async Task ParallelForEachAsync<T>(IEnumerable<T> collection, Action<T> action)
        {
            await Task.Run(() => Parallel.ForEach<T>(collection, action));
        }

        /// <summary>
        /// Basically an asynchronous wrapper for the Parallel.For method.
        /// </summary>
        /// <param name="fromInclusive">The number to start the For-loop from.</param>
        /// <param name="toExclusive">The number coming after the last number in the loop.</param>
        /// <param name="action">The action to perform - in parallel - for each number.</param>
        /// <returns>A Task object for the operation.</returns>
        public static async Task ParallelForAsync(long fromInclusive, long toExclusive, Action<long> action)
        {
            await Task.Run(() => Parallel.For(fromInclusive, toExclusive, action));
        }

        /// <summary>
        /// Calls an asynchronous function for each item in a collection, allowing up to a specified number of Tasks to
        /// run at the same time.
        /// </summary>
        /// <typeparam name="T">The type of the items in the collection.</typeparam>
        /// <param name="collection">The collection of items to perform a function with.</param>
        /// <param name="funcAsync">The function to call for each item.</param>
        /// <param name="maxConcurrentTasks">The maximum number of Tasks running at the same time.</param>
        /// <returns>A Task object for the operation.</returns>
        public static async Task ForEachAsync<T>(IEnumerable<T> collection, Func<T, Task> funcAsync, Byte maxConcurrentTasks)
        {
            byte concArraySize = (byte)Math.Min(collection.Count(), maxConcurrentTasks);
            Task[] concurrentTasks = new Task[concArraySize];

            using (IEnumerator<T> enumerator = collection.GetEnumerator())
            {
                for (byte idx = 0; idx < concArraySize; idx++)
                {
                    enumerator.MoveNext();
                    concurrentTasks[idx] = funcAsync(enumerator.Current);
                }

                while (enumerator.MoveNext())
                {
                    Task finishedTask = await Task.WhenAny(concurrentTasks);
                    int opening = Array.IndexOf(concurrentTasks, finishedTask);
                    concurrentTasks[opening] = funcAsync(enumerator.Current);
                }

                await Task.WhenAll(concurrentTasks);
            }
        }

        /// <summary>
        /// Calls an asynchronous function for each item in a collection, allowing up to a specified number of Tasks to
        /// run at the same time.
        /// </summary>
        /// <typeparam name="T">The type of the items in the collection.</typeparam>
        /// <typeparam name="TContext">The type of a context passed to the function that is called for each item.</typeparam>
        /// <param name="context">The context passed to the function called for each item.</param>
        /// <param name="collection">The collection of items to perform a function with.</param>
        /// <param name="funcAsync">The function to call for each item.</param>
        /// <param name="maxConcurrentTasks">The maximum number of Tasks running at the same time.</param>
        /// <returns>A Task object for the operation.</returns>
        public static async Task ForEachAsync<T, TContext>(TContext context, IEnumerable<T> collection, Func<TContext, T, Task> funcAsync, Byte maxConcurrentTasks)
        {
            byte concArraySize = (byte)Math.Min(collection.Count(), maxConcurrentTasks);
            Task[] concurrentTasks = new Task[concArraySize];

            using (IEnumerator<T> enumerator = collection.GetEnumerator())
            {
                for (byte idx = 0; idx < concArraySize; idx++)
                {
                    enumerator.MoveNext();
                    concurrentTasks[idx] = funcAsync(context, enumerator.Current);
                }

                while (enumerator.MoveNext())
                {
                    Task finishedTask = await Task.WhenAny(concurrentTasks);
                    int opening = Array.IndexOf(concurrentTasks, finishedTask);
                    concurrentTasks[opening] = funcAsync(context, enumerator.Current);
                }

                await Task.WhenAll(concurrentTasks);
            }
        }

        /// <summary>
        /// Calls an asynchronous function for each item in a collection, allowing up to a specified number of Tasks to
        /// run at the same time. Returns an array of Tuples, consisting of the items in the collection, together with the result
        /// from the function called for each item. The order of the array will be the same as in the input collection.
        /// </summary>
        /// <typeparam name="T">The type of the items in the collection.</typeparam>
        /// <typeparam name="TResult">The type of the return value from the function called for each item.</typeparam>
        /// <typeparam name="TContext">The type of a context passed to the function that is called for each item.</typeparam>
        /// <param name="context">The context passed to the function called for each item.</param>
        /// <param name="collection">The collection of items to perform a function with.</param>
        /// <param name="funcAsync">The function to call for each item.</param>
        /// <param name="maxConcurrentTasks">The maximum number of Tasks running at the same time.</param>
        /// <returns>An array of Tuples, consisting of the items in the collection, together with the result
        /// from the function called for each item. The order of the array will be the same as in the input collection.</returns>
        public static async Task<Tuple<T, TResult>[]> ForEachAsync<T, TResult, TContext>(TContext context, IEnumerable<T> collection, Func<TContext, T, Task<TResult>> funcAsync, Byte maxConcurrentTasks)
        {
            Tuple<T, TResult>[] returnArray = new Tuple<T, TResult>[collection.Count()];
            int count = 0;

            byte concArraySize = (byte)Math.Min(collection.Count(), maxConcurrentTasks);
            Task<TResult>[] concurrentTasks = new Task<TResult>[concArraySize];
            Tuple<T, int>[] indices = new Tuple<T, int>[concArraySize];

            using (IEnumerator<T> enumerator = collection.GetEnumerator())
            {
                for (byte idx = 0; idx < concArraySize; idx++)
                {
                    enumerator.MoveNext();
                    concurrentTasks[idx] = funcAsync(context, enumerator.Current);
                    indices[idx] = new Tuple<T, int>(enumerator.Current, count++);
                }

                while (enumerator.MoveNext())
                {
                    Task<TResult> finishedTask = await Task.WhenAny(concurrentTasks);

                    int opening = Array.IndexOf(concurrentTasks, finishedTask);
                    returnArray[indices[opening].Item2] = new Tuple<T, TResult>(indices[opening].Item1, finishedTask.Result);
                    concurrentTasks[opening] = funcAsync(context, enumerator.Current);
                    indices[opening] = new Tuple<T, int>(enumerator.Current, count++);
                }

                List<Task<TResult>> remainingTasks = concurrentTasks.ToList();
                List<Tuple<T, int>> remainingIndices = indices.ToList();
                while (remainingTasks.Count > 0)
                {
                    Task<TResult> finishedTask = await Task.WhenAny(remainingTasks.ToArray());
                    int finishedTaskIdx = remainingTasks.IndexOf(finishedTask);
                    returnArray[remainingIndices[finishedTaskIdx].Item2] = new Tuple<T, TResult>(remainingIndices[finishedTaskIdx].Item1, finishedTask.Result);

                    remainingTasks.RemoveAt(finishedTaskIdx);
                    remainingIndices.RemoveAt(finishedTaskIdx);
                }
            }

            return returnArray;
        }
    }
}
