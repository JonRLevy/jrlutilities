﻿using JonRLevy.JRLUtilities.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.JRLUtilities.Cache
{
    /// <summary>
    /// Enumeration for the various Cache shrinkage strategies
    /// </summary>
    public enum CacheShrinkageScheme
    {
        /// <summary>
        /// Shrinkage strategy where those items most recently requested are kept
        /// </summary>
        LatestRequest,

        /// <summary>
        /// Shrinkage strategy where the most requested items are kept
        /// </summary>
        RequestCount,

        /// <summary>
        /// Shrinkage strategy where those items with the highest average number of requests per minute are kept
        /// </summary>
        RequestsPerMinute,

        /// <summary>
        /// Shrinkage where it is randomly determine which items to keep in the cache
        /// </summary>
        Random
    }

    /// <summary>
    /// A generic Cache for storing items locally, and retrieving them from "somewhere" when not found. Various options can specify how long an items
    /// is allowed to stay in the cache, and which items to keep when the cache is shrunk.
    /// </summary>
    /// <typeparam name="TKey">The type used to identify items in the cache.</typeparam>
    /// <typeparam name="TValue">The type of items in the cache. Must be a Class.</typeparam>
    /// <typeparam name="TContext">The Context is passed on the delegate actually Fetching the data when it is not found in the cache. Might for instance be a database connection. This is it's type.</typeparam>
    public class Cache<TKey, TValue, TContext> where TValue: class
    {
        class CacheItem
        {
            public DateTimeOffset FirstFetched { get; set; }
            public DateTimeOffset LastRequested { get; set; }
            public uint RequestCount { get; set; }
            public TValue Item { get; set; }

            private CacheItem()
            { }

            public CacheItem(TValue item)
            {
                FirstFetched = LastRequested = DateTimeOffset.UtcNow;
                RequestCount = 1;
                Item = item;
            }
        }

        Dictionary<TKey, CacheItem> storage = new Dictionary<TKey, CacheItem>();

        /// <summary>
        /// This delegate retrieves an item not found in the cache.
        /// </summary>
        /// <param name="DataContext">The Context the delegate can use to retrieve the item.</param>
        /// <param name="DataKey">The Key/Identifier for the item.</param>
        /// <returns>An item. May be null. Depending on the value for StoreNull nulls may be kept in the cache.</returns>
        public delegate Task<TValue> FetchRequestAsync(TContext DataContext, TKey DataKey);

        /// <summary>
        /// The maximum no of items to keep in the cache. When the cache grows beyond this it is shrunk using the current shrinkage strategy,
        /// to the ShrinkTo size.
        /// </summary>
        public int MaxItems { get; set; } = 50;

        /// <summary>
        /// The size to reduce the cache to when shrunk. Items kept depends on the current shrankage strategy.
        /// </summary>
        public int ShrinkTo { get; set; } = 25;

        /// <summary>
        /// The number of items currently in the cache.
        /// </summary>
        public int CurrentItemCount
        {
            get
            {
                lock (storage)
                {
                    return storage.Count;
                }
            }
        }

        /// <summary>
        /// A TimeSpan specifying how long an item in the cache is considered valid. After this time a new version is fetched when requested.
        /// If this is null, it means items never become stale.
        /// </summary>
        public TimeSpan? StaleAfter { get; set; } = null;

        /// <summary>
        /// The number of times an items has been found locally in the cache.
        /// </summary>
        public int Hits { get; set; } = 0;

        /// <summary>
        /// The number of times an item has not been found locally in the cache, resulting in an attempt to fetch it.
        /// </summary>
        public int Miss { get; set; } = 0;

        private bool storeNull = true;
        /// <summary>
        /// Determines if attempts to get a value that results in a null is stored in the cache; thus not having to look again.
        /// </summary>
        public bool StoreNull
        {
            get { return storeNull; }
            set
            {
                storeNull = value;
                if (value == false)
                {
                    lock (storage)
                    {
                        DeferredRemover<KeyValuePair<TKey, CacheItem>> dr = new DeferredRemover<KeyValuePair<TKey, CacheItem>>(storage);
                        dr.RemoveDeferredRange(storage.Where(kvp => kvp.Value.Item==null));
                        dr.Remove();
                    }
                }
            }
        }

        /// <summary>
        /// Determines which strategy to use when shrinking the cache, for determining which items to keep.
        /// </summary>
        public CacheShrinkageScheme ShrinkageScheme { get; set; } = CacheShrinkageScheme.LatestRequest;

        /// <summary>
        /// The number of Hits divided by the number of Miss'es. Is zero if misses is zero.
        /// </summary>
        public double HitMissRatio => 
            Miss == 0 ? 0.0 : (double)Hits / (double)Miss;

        /// <summary>
        /// The delegate called for finding an item, if none are found in the cache, unless another delegate is called.
        /// If this is not set, then a delegate must be specified in the request.
        /// </summary>
        public FetchRequestAsync DefaultFetchRequestAsync { get; set; }

        /// <summary>
        /// Try to find an item in the cache; if it is not found the DefaultFetchRequestAsync is called to try to find it.
        /// </summary>
        /// <param name="Context">The DataContext passed to DefaultFetchRequestAsync</param>
        /// <param name="Key">The key identifying the item.</param>
        /// <returns>The value found, or null if nothing is found.</returns>
        public async Task<TValue> GetAsync(TContext Context, TKey Key)
        {
            TValue result;
            if (getLocal(Key, out result))
                return result;

            return await doFetchAsync(Context, Key, DefaultFetchRequestAsync);
        }

        /// <summary>
        /// Try to find an item in the cache; if it is not found the provided FetchRequestAsync is called to try to find it.
        /// </summary>
        /// <param name="Context">The DataContext passed to FetchRequestAsync</param>
        /// <param name="Key">The key identifying the item.</param>
        /// <param name="FetchRequestAsync">The FetchRequestAsync delegate to call to fetch an item not found locally</param>
        /// <returns>The value found, or null if nothing is found.</returns>
        public async Task<TValue> GetAsync(TContext Context, TKey Key, FetchRequestAsync FetchRequestAsync)
        {
            TValue result;
            if (getLocal(Key, out result))
                return result;

            return await doFetchAsync(Context, Key, FetchRequestAsync);
        }

        private bool getLocal(TKey Key, out TValue local)
        {
            lock (storage)
            {
                CacheItem ci;

                if (storage.TryGetValue(Key, out ci))
                {
                    if (StaleAfter.HasValue)
                    {
                        if (ci.FirstFetched + StaleAfter < DateTimeOffset.UtcNow)
                        {
                            storage.Remove(Key);
                            local = null;
                            return false;
                        }
                    }

                    Hits++;
                    ci.LastRequested = DateTimeOffset.Now;
                    ci.RequestCount++;
                    local = ci.Item;
                    return true;
                }
            }

            local = null;
            return false;
        }

        private async Task<TValue> doFetchAsync(TContext Context, TKey Key, FetchRequestAsync FetchRequestAsync)
        {
            TValue item = await FetchRequestAsync(Context, Key);

            lock (storage)
            {
                Miss++;

                CacheItem ci;

                if (storage.TryGetValue(Key, out ci))
                {
                    ci.LastRequested = DateTimeOffset.Now;
                    ci.RequestCount++;
                    return ci.Item;
                }
                else
                {
                    if (item != null || storeNull)
                    {
                        storage[Key] = new CacheItem(item);

                        if (storage.Count > MaxItems)
                            ShrinkAsync();
                    }

                    return item;
                }
            }
        }

        /// <summary>
        /// Forces a Shrink of the cache to the ShrinkTo size, even if the cache hasn't grown beyond MaxItems.
        /// Uses the current ShrinkageScheme to determine which items to keep.
        /// </summary>
        public async void ShrinkAsync()
        {
            await Task.Delay(10);

            lock (storage)
            {
                DeferredRemover<KeyValuePair<TKey, CacheItem>> dr = new DeferredRemover<KeyValuePair<TKey, CacheItem>>(storage);
                if (StaleAfter.HasValue)
                {
                    DateTimeOffset cutoff = DateTimeOffset.UtcNow + StaleAfter.Value;
                    dr.RemoveDeferredRange(storage.Where(kvp => kvp.Value.FirstFetched < cutoff));
                    dr.Remove();
                }

                int ItemsToRemove = storage.Count - ShrinkTo;
                
                switch (ShrinkageScheme)
                {
                    case CacheShrinkageScheme.Random:
                        Random rand = new Random();
                        dr.RemoveDeferredRange(storage.OrderBy(kvp => rand.Next()).Take(ItemsToRemove));
                        break;
                    case CacheShrinkageScheme.RequestsPerMinute:
                        DateTimeOffset now = DateTimeOffset.UtcNow;
                        dr.RemoveDeferredRange(storage.OrderByDescending(kvp => (now - kvp.Value.FirstFetched).TotalMilliseconds / kvp.Value.RequestCount).Take(ItemsToRemove));
                        break;
                    case CacheShrinkageScheme.RequestCount:
                        dr.RemoveDeferredRange(storage.OrderBy(kvp => kvp.Value.RequestCount).Take(ItemsToRemove));
                        break;
                    case CacheShrinkageScheme.LatestRequest:
                    default:
                        dr.RemoveDeferredRange(storage.OrderBy(kvp => kvp.Value.LastRequested).Take(ItemsToRemove));
                        break;
                }
                dr.Remove();
            }
        }
    }
}
