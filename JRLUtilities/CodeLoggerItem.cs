﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.JRLUtilities.Logging
{
    /// <summary>
    /// Creates an item for logging from a piece of code. Logs to the provided ICodeLogger. Automatically logs
    /// entry (on creation) and exit (on disposal) - meaning, when used in a using statement it will log entering
    /// and exit of that code block, as well as any specific messages or exceptions logged.
    /// </summary>
    public class CodeLoggerItem : IDisposable
    {
        ICodeLogger codeLogger;
        CodeLogImportance baseImportance;
        DateTimeOffset codeEntry = DateTimeOffset.UtcNow;
        List<string> tags = new List<string>();
        string codeBlockName;

        private CodeLoggerItem() { }

        /// <summary>
        /// Creates a CodeLoggerItem, logging messages to the provided ICodeLogger, for this block of code.
        /// </summary>
        /// <param name="codeLogger">The ICodeLogger to log messages to. If null nothing will be logged.</param>
        /// <param name="importance">The base level of imporatnce assigned to messages from this CodeLoggerItem. May be raised
        /// for specific messages, but can't be lowered. Dfeault is Notification.</param>
        /// <param name="codeBlockName">A name assigned to the CodeBlockerItem; default is the calling methods name.</param>
        /// <param name="tags">An enumeration of tags to use for messages logged.</param>
        public CodeLoggerItem(ICodeLogger codeLogger, CodeLogImportance importance = CodeLogImportance.Notification, [CallerMemberName] string codeBlockName="", IEnumerable<string> tags=null)
        {
            this.codeLogger = codeLogger;
            this.codeBlockName = codeBlockName;
            baseImportance = importance;
            if (tags != null)
                this.tags.AddRange(tags);

            codeLogger?.Log(codeBlockName, baseImportance, this.tags, codeEntry, codeEntry - codeEntry, "Entering");            
        }

        /// <summary>
        /// Disposing of a CodeLoggerItem will send a message to the ICodeLogger, saying the codeblock is exited.
        /// </summary>
        public void Dispose()
        {
            codeLogger?.Log(codeBlockName, baseImportance, tags, codeEntry, DateTimeOffset.UtcNow - codeEntry, "Exiting");
        }

        /// <summary>
        /// Log a message
        /// </summary>
        /// <param name="logMessage">The message to log</param>
        /// <param name="importance">The importance to assign to the message. Will at least be the baseline importance of the CodeLoggerItem</param>
        /// <param name="tags">An enumeration of tags; these will be combined with any provided when creating the CodeLoggerItem</param>
        public void Log(string logMessage, CodeLogImportance? importance = null, IEnumerable<string> tags = null)
        {
            CodeLogImportance usedImportance = baseImportance;
            if (importance.HasValue && importance.Value > usedImportance)
                usedImportance = importance.Value;

            List<string> usedTags;
            if (tags == null)
                usedTags = this.tags;
            else
                usedTags = this.tags.Union(tags).ToList();

            codeLogger?.Log(codeBlockName, usedImportance, usedTags, codeEntry, DateTimeOffset.UtcNow - codeEntry, logMessage);
        }

        /// <summary>
        /// Log an exception
        /// </summary>
        /// <param name="exception">The exception object to log</param>
        /// <param name="where">Specifically where in the code; the default is the linenumber of the caller</param>
        /// <returns></returns>
        public Exception LogException(Exception exception, [CallerLineNumber] int where=0)
        {
            codeLogger.LogException(String.Format("{0} (Line {1})", codeBlockName, where), tags, DateTimeOffset.UtcNow, DateTimeOffset.UtcNow - codeEntry, exception);

            return exception;
        }
    }
}
