﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JonRLevy.JRLUtilities.Logging
{
    /// <summary>
    /// An implementation of ICodeLogger, that logs to the Console
    /// </summary>
    public class ConsoleCodeLogger : ICodeLogger
    {
        /// <summary>
        /// If included tags is null, all messages with tags not excluded are included. If not null, only those
        /// messages containing tags enumerated are included (and only if not also excluded).
        /// </summary>
        public IEnumerable<string> IncludedTags { get; set; }
        /// <summary>
        /// Messages containing excluded tags will not be logged.
        /// </summary>
        public IEnumerable<string> ExcludedTags { get; set; }

        /// <summary>
        /// Sets the least level of importance to log.
        /// </summary>
        public CodeLogImportance MinimumImportance { get; set; } = CodeLogImportance.Notification;

        /// <summary>
        /// Creates a ConsoleCodeLogger
        /// </summary>
        /// <param name="minimumImportance">Sets the least level of importance to log. Dfeault is Notification.</param>
        /// <param name="includedTags">Sets enumeration of tags to include. Default is null.</param>
        /// <param name="excludedTags">Sets enumeration of tags to exclude. Default is null.</param>
        public ConsoleCodeLogger(CodeLogImportance minimumImportance = CodeLogImportance.Notification, IEnumerable<string> includedTags = null, IEnumerable<string> excludedTags = null)
        {
            MinimumImportance = minimumImportance;
            IncludedTags = includedTags;
            ExcludedTags = excludedTags;
        }

        /// <summary>
        /// Log a message
        /// </summary>
        /// <param name="from">The place in the code logging the message</param>
        /// <param name="importance">The importance assigned to this message (for filtering)</param>
        /// <param name="tags">Any tags assigned to this message (for filtering)</param>
        /// <param name="dateTime">The time that should be logged</param>
        /// <param name="relAfter">A TimeSpan specifying time since logging for this piece of code started</param>
        /// <param name="Message">The message to log</param>
        public void Log(string from, CodeLogImportance importance, IEnumerable<string> tags, DateTimeOffset dateTime, TimeSpan relAfter, string Message)
        {
            if (!checkRelevance(importance, tags))
                return;

            Console.WriteLine("{0} {1} after {2}: {3}", dateTime, from, relAfter, Message);
        }

        /// <summary>
        /// Log an Exception
        /// </summary>
        /// <param name="from">The place in the code logging the message</param>
        /// <param name="tags">Any tags assigned to this message (for filtering)</param>
        /// <param name="dateTime">The time that should be logged</param>
        /// <param name="relAfter">A TimeSpan specifying time since logging for this piece of code started</param>
        /// <param name="exception">An Exception object to log</param>
        public void LogException(string from, IEnumerable<string> tags, DateTimeOffset dateTime, TimeSpan relAfter, Exception exception)
        {
            if (!checkRelevance(CodeLogImportance.Error, tags))
                return;

            Console.WriteLine("{0} {1} after {2}: {3}", dateTime, from, relAfter, exception.ToString());
        }

        private bool checkRelevance(CodeLogImportance importance, IEnumerable<string> tags)
        {
            if (importance < MinimumImportance)
                return false;

            if (IncludedTags != null)
                if (!IncludedTags.Any(tags.Contains))  //could also use IncludedTags.Intersect(tags).Any()
                    return false;

            if (ExcludedTags != null)
                if (ExcludedTags.Any(tags.Contains))
                    return false;

            return true;
        }
    }
}
