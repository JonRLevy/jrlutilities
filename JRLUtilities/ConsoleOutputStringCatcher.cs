﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.JRLUtilities.IO
{
    /// <summary>
    /// Catches all output to the Console into a string, until Disposed, when the Console output is restored.
    /// </summary>
    public class ConsoleOutputStringCatcher : IDisposable
    {
        private StringWriter stringWriter;
        private TextWriter originalOutput;

        /// <summary>
        /// Creates a StringCatcher which will automatically capture all output to the Console into a String,
        /// until Disposed. Hint: use in a using-statement.
        /// </summary>
        public ConsoleOutputStringCatcher()
        {
            stringWriter = new StringWriter();
            originalOutput = Console.Out;
            Console.SetOut(stringWriter);
        }

        /// <summary>
        /// Empties the StringCatcher of all output caught so far.
        /// </summary>
        public void ClearOutput()
        {
            StringWriter prevStringWriter = stringWriter;
            Console.SetOut(stringWriter = new StringWriter());
            prevStringWriter.Dispose();
        }

        /// <summary>
        /// Get the console output caught so far.
        /// </summary>
        /// <returns>The caught output as a string.</returns>
        public string GetStringOuput()
        {
            return stringWriter.ToString();
        }

        /// <summary>
        /// Resets the output for the Console, and disposes of the internal StringWriter.
        /// </summary>
        public void Dispose()
        {
            Console.SetOut(originalOutput);
            stringWriter.Dispose();
        }
    }
}