﻿using System;
using System.Data;

namespace JonRLevy.JRLUtilities.Database
{
    /// <summary>
    /// An extension class for the <c>System.Data.DataReader</c> class, provides methods to retrieve
    /// data with default values in case of DBNulls, and nullable types (with nulls for DBNulls).
    /// </summary>
    public static class DataReaderEx
    {
        /// <summary>
        /// Get a <c>bool</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>bool</c>, or the defaultvalue in case of DBNull</returns>
        public static bool GetBoolean(this IDataReader datareader, int i, bool defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetBoolean(i);

        /// <summary>
        /// Get a <c>bool?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>bool</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static bool? GetNullableBoolean(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (bool?)null : datareader.GetBoolean(i);


        /// <summary>
        /// Get a <c>byte</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>byte</c>, or the defaultvalue in case of DBNull</returns>
        public static byte GetByte(this IDataReader datareader, int i, byte defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetByte(i);

        /// <summary>
        /// Get a <c>byte?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>byte</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static byte? GetNullableByte(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (byte?)null : datareader.GetByte(i);


        /// <summary>
        /// Get a <c>char</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>char</c>, or the defaultvalue in case of DBNull</returns>
        public static char GetChar(this IDataReader datareader, int i, char defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetChar(i);

        /// <summary>
        /// Get a <c>char?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>char</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static char? GetNullableChar(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (char?)null : datareader.GetChar(i);


        /// <summary>
        /// Get a <c>DateTime</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>DateTime</c>, or the defaultvalue in case of DBNull</returns>
        public static DateTime GetDateTime(this IDataReader datareader, int i, DateTime defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetDateTime(i);

        /// <summary>
        /// Get a <c>DateTime?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>DateTime</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static DateTime? GetNullableDateTime(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (DateTime?)null : datareader.GetDateTime(i);


        /// <summary>
        /// Get a <c>decimal</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>decimal</c>, or the defaultvalue in case of DBNull</returns>
        public static decimal GetDecimal(this IDataReader datareader, int i, decimal defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetDecimal(i);

        /// <summary>
        /// Get a <c>decimal?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>decimal</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static decimal? GetNullableDecimal(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (decimal?)null : datareader.GetDecimal(i);


        /// <summary>
        /// Get a <c>double</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>double</c>, or the defaultvalue in case of DBNull</returns>
        public static double GetDouble(this IDataReader datareader, int i, double defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetDouble(i);

        /// <summary>
        /// Get a <c>double?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>double</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static double? GetNullableDouble(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (double?)null : datareader.GetDouble(i);


        /// <summary>
        /// Get a <c>float</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>float</c>, or the defaultvalue in case of DBNull</returns>
        public static float GetFloat(this IDataReader datareader, int i, float defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetFloat(i);

        /// <summary>
        /// Get a <c>float?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>float</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static float? GetNullableFloat(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (float?)null : datareader.GetFloat(i);


        /// <summary>
        /// Get a <c>short</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>short</c>, or the defaultvalue in case of DBNull</returns>
        public static short GetInt16(this IDataReader datareader, int i, short defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetInt16(i);

        /// <summary>
        /// Get a <c>short?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>short</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static short? GetNullableInt16(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (short?)null : datareader.GetInt16(i);


        /// <summary>
        /// Get an <c>int</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>int</c>, or the defaultvalue in case of DBNull</returns>
        public static int GetInt32(this IDataReader datareader, int i, int defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetInt32(i);

        /// <summary>
        /// Get an <c>int?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>int</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static int? GetNullableInt32(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (int?)null : datareader.GetInt32(i);


        /// <summary>
        /// Get a <c>long</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The value to return in case the value is DBNull</param>
        /// <returns>The value in the specified column cast as <c>long</c>, or the defaultvalue in case of DBNull</returns>
        public static long GetInt64(this IDataReader datareader, int i, long defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetInt64(i);

        /// <summary>
        /// Get a <c>long?</c> value for the column index of the <c>IDataReader</c>. Returns the value, or null in case
        /// the column contains DBNull.
        /// </summary>
        /// <param name="datareader">The <c>IDataReader</c> to get a value from.</param>
        /// <param name="i">The index of the column.</param>
        /// <returns>The <c>long</c> value in the specified column; or null if the column contains DBNull.</returns>
        public static long? GetNullableInt64(this IDataReader datareader, int i)
            => datareader.IsDBNull(i) ? (long?)null : datareader.GetInt64(i);


        /// <summary>
        /// Get an <c>Object</c> value for the column index of the datareader. Return the provided defaultvalue in case of DBNull.
        /// </summary>
        /// <param name="datareader">The DataReader to get a value from.</param>
        /// <param name="i">The index of the column</param>
        /// <param name="defaultvalue">The <c>Object</c> to return in case the value is DBNull</param>
        /// <returns>The <c>Object</c>, or the defaultvalue in case of DBNull</returns>
        public static Object GetValue(this IDataReader datareader, int i, Object defaultvalue)
            => datareader.IsDBNull(i) ? defaultvalue : datareader.GetValue(i);
    }
}
