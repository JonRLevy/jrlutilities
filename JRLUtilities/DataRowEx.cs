﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.JRLUtilities.Database
{
    /// <summary>
    /// Provides extension methods for the <c>System.Data.DataRow</c>-class, for conveniently retrieving typed columns,
    /// and defaults in case of DBNulls, or a nullable type.
    /// </summary>
    public static class DataRowEx
    {
        /// <summary>
        /// Return a column in a DataRow cast as a <c>bool</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>bool</c></returns>
        public static bool GetBoolean(this DataRow dataRow, string columnName)
            => (bool)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>bool?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>bool?</c> - if the column contains DBNull, null is returned</returns>
        public static bool? GetNullableBoolean(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (bool?)null : (bool)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>bool</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>bool</c>, or the defaultValue.</returns>
        public static bool GetBoolean(this DataRow dataRow, string columnName, bool defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (bool)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>byte</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>byte</c></returns>
        public static byte GetByte(this DataRow dataRow, string columnName)
            => (byte)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>byte?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>byte?</c> - if the column contains DBNull, null is returned</returns>
        public static byte? GetNullableByte(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (byte?)null : (byte)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>byte</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>byte</c>, or the defaultValue.</returns>
        public static byte GetByte(this DataRow dataRow, string columnName, byte defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (byte)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>short</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>short</c></returns>
        public static short GetInt16(this DataRow dataRow, string columnName)
            => (short)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>short?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>short?</c> - if the column contains DBNull, null is returned</returns>
        public static short? GetNullableInt16(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (short?)null : (short)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>short</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>short</c>, or the defaultValue.</returns>
        public static short GetInt16(this DataRow dataRow, string columnName, short defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (short)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as an <c>int</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as an <c>int</c></returns>
        public static int GetInt32(this DataRow dataRow, string columnName)
            => (int)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as an <c>int?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as an <c>int?</c> - if the column contains DBNull, null is returned</returns>
        public static int? GetNullableInt32(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (int?)null : (int)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as an <c>int</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as an <c>int</c>, or the defaultValue.</returns>
        public static int GetInt32(this DataRow dataRow, string columnName, int defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (int)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>long</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>long</c></returns>
        public static long GetInt64(this DataRow dataRow, string columnName)
            => (long)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>long?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>long?</c> - if the column contains DBNull, null is returned</returns>
        public static long? GetNullableInt64(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (long?)null : (long)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>long</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>long</c>, or the defaultValue.</returns>
        public static long GetInt64(this DataRow dataRow, string columnName, long defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (long)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>decimal</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>decimal</c></returns>
        public static decimal GetDecimal(this DataRow dataRow, string columnName)
            => (decimal)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>decimal?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>decimal?</c> - if the column contains DBNull, null is returned</returns>
        public static decimal? GetNullableDecimal(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (decimal?)null : (decimal)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>decimal</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>decimal</c>, or the defaultValue.</returns>
        public static decimal GetDecimal(this DataRow dataRow, string columnName, decimal defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (decimal)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>double</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>double</c></returns>
        public static double GetDouble(this DataRow dataRow, string columnName)
            => (double)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>double?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>double?</c> - if the column contains DBNull, null is returned</returns>
        public static double? GetNullableDouble(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (double?)null : (double)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>double</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>double</c>, or the defaultValue.</returns>
        public static double GetDouble(this DataRow dataRow, string columnName, double defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (double)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>float</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>float</c></returns>
        public static float GetSingle(this DataRow dataRow, string columnName)
            => (float)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>float?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>float?</c> - if the column contains DBNull, null is returned</returns>
        public static float? GetNullableSingle(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (float?)null : (float)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>float</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>float</c>, or the defaultValue.</returns>
        public static float GetSingle(this DataRow dataRow, string columnName, float defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (float)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>DateTimeOffset</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>DateTimeOffset</c></returns>
        public static DateTimeOffset GetDateTimeOffset(this DataRow dataRow, string columnName)
            => (DateTimeOffset)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>DateTimeOffset?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>DateTimeOffset?</c> - if the column contains DBNull, null is returned</returns>
        public static DateTimeOffset? GetNullableDateTimeOffset(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (DateTimeOffset?)null : (DateTimeOffset)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>DateTimeOffset</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>DateTimeOffset</c>, or the defaultValue.</returns>
        public static DateTimeOffset GetDateTimeOffset(this DataRow dataRow, string columnName, DateTimeOffset defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (DateTimeOffset)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>DateTime</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>DateTime</c></returns>
        public static DateTime GetDateTime(this DataRow dataRow, string columnName)
            => (DateTime)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>DateTime?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>DateTimeOffset?</c> - if the column contains DBNull, null is returned</returns>
        public static DateTime? GetNullableDateTime(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (DateTime?)null : (DateTime)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>DateTime</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>DateTime</c>, or the defaultValue.</returns>
        public static DateTime GetDateTime(this DataRow dataRow, string columnName, DateTime defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (DateTime)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>TimeSpan</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>TimeSpan</c></returns>
        public static TimeSpan GetTimeSpan(this DataRow dataRow, string columnName)
            => (TimeSpan)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>TimeSpan?</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>TimeSpan?</c> - if the column contains DBNull, null is returned</returns>
        public static TimeSpan? GetNullableTimeSpan(this DataRow dataRow, string columnName)
            => dataRow.IsNull(columnName) ? (TimeSpan?)null : (TimeSpan)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>TimeSpan</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>TimeSpan</c>, or the defaultValue.</returns>
        public static TimeSpan GetTimeSpan(this DataRow dataRow, string columnName, TimeSpan defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (TimeSpan)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>string</c>.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <returns>The content of the column, cast as a <c>string</c></returns>
        public static string GetString(this DataRow dataRow, string columnName)
            => (string)dataRow[columnName];

        /// <summary>
        /// Return a column in a DataRow cast as a <c>string</c>. In case of a DBNull, return the provided defaultvalue instead.
        /// </summary>
        /// <param name="dataRow">The DataRow to return a column from.</param>
        /// <param name="columnName">The name of the column to return.</param>
        /// <param name="defaultValue">The value to return in case the column is null.</param>
        /// <returns>The content of the column cast as a <c>string</c>, or the defaultValue.</returns>
        public static string GetString(this DataRow dataRow, string columnName, string defaultValue)
            => dataRow.IsNull(columnName) ? defaultValue : (string)dataRow[columnName];
    }
}
