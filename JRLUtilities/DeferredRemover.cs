﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.JRLUtilities.Collections.Generic
{
    /// <summary>
    /// The DeferredRemover can be used with an ICollection to remove items at a later point. Convenient when you do a foreach on a Collection,
    /// and you want to remove some items, but are not able to do so as part of the foreach.
    /// <para>When you create the DeferredRemover you give it the Collection it should remove obects from. Call RemoveDeferred or RemoveDeferredRange to
    /// add items thats should be removed.</para>
    /// <para>Call Remove to actually remove the items. Or Regret to cancel all removals done so far.</para>
    /// <para>Note that nothing is done to optimize removal of items; Remove are simply called on the Collection for each item added, in the same order
    /// they were added, to simulate them being Removed at the time.</para>
    /// </summary>
    /// <example>
    /// This example shows how to use a DeferredRemover
    /// <code>
    /// using System;
    /// using System.Collections.Generic;
    /// using JonRLevy.JRLUtilities.Collections.Generic;
    /// 
    /// class Example
    /// {
    ///     public static void Main()
    ///     {
    ///         List&lt;int&gt; OddList=new List&lt;int&gt;();
    ///         OddList.Add(1);
    ///         OddList.Add(2);
    ///         OddList.Add(3);
    ///         OddList.Add(4);
    ///         OddList.Add(5);
    ///         
    ///         List&lt;int&gt; EvenList=new List&lt;int&gt;();
    /// 
    ///         DeferredRemover&lt;int&gt; dr=new DeferredRemover&lt;int&gt;(OddList);
    ///         
    ///         foreach (int number in OddList)
    ///         {
    ///             if (number % 2 == 0)
    ///                 EvenList.Add(dr.RemoveDeferred(number));
    ///         }
    ///         
    ///         dr.Remove();
    ///     }
    /// }
    /// </code>
    /// </example>
    /// <typeparam name="T">The Type of the items in the Collection</typeparam>
    public class DeferredRemover<T>
    {
        ICollection<T> theCollection;
        List<T> toBeRemoved;

        private DeferredRemover() //should not get called
        {
        }

        /// <summary>
        /// Create a DeferredRemover to add items to, from a collection for later removal
        /// </summary>
        /// <param name="ICollection">The Collection Items should be removed from</param>
        public DeferredRemover(ICollection<T> ICollection)
        {
            theCollection = ICollection;
            toBeRemoved = new List<T>();
        }

        /// <summary>
        /// Add an item to remove at a later point
        /// </summary>
        /// <param name="Item">The item to remove later</param>
        /// <returns></returns>
        public T RemoveDeferred(T Item)
        {
            toBeRemoved?.Add(Item);

            return Item;
        }

        /// <summary>
        /// Add a range of items to remove at a later point
        /// </summary>
        /// <param name="range">The range of items to remove</param>
        public void RemoveDeferredRange(IEnumerable<T> range)
        {
            toBeRemoved?.AddRange(range);
        }

        /// <summary>
        /// Regret all removal requests done so far
        /// </summary>
        public void Regret()
        {
            toBeRemoved.Clear();
        }

        /// <summary>
        /// Do the actual removal of all items requested so far
        /// </summary>
        /// <returns>A count of items not removed (that is, returning false from Collection &lt;T&gt;.Remove)"/></returns>
        public int Remove()
        {
            if (theCollection == null)
            {
                toBeRemoved.Clear();
                return -1;
            }

            int nonRemoved = 0;
            foreach (T item in toBeRemoved)
            {
                if (!theCollection.Remove(item))
                    nonRemoved++;
            }

            toBeRemoved.Clear();
            return nonRemoved;
        }
    }
}
