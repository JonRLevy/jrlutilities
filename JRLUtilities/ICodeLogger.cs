﻿using System;
using System.Collections.Generic;

namespace JonRLevy.JRLUtilities.Logging
{
    /// <summary>
    /// Enumerates the various levels of importance that can be assigned to log messages.
    /// </summary>
    public enum CodeLogImportance
    {
        /// <summary>
        /// A notification - the lowest importance
        /// </summary>
        Notification,
        /// <summary>
        /// A warning - nothing severe
        /// </summary>
        Warning,
        /// <summary>
        /// An error - something that should be handled or looked into.
        /// </summary>
        Error,
        /// <summary>
        /// A fatal error - something that makes a program stop
        /// </summary>
        Fatal
    }

    /// <summary>
    /// Implementers provide a place to log messages to, for CodeLoggerItems.
    /// </summary>
    public interface ICodeLogger
    {
        /// <summary>
        /// If included tags is null, all messages with tags not excluded should be included. If not null, only those
        /// messages containing tags enumerated should be included (and only if not also excluded).
        /// </summary>
        IEnumerable<string> IncludedTags { get; set; }

        /// <summary>
        /// Messages containing excluded tags will not be logged.
        /// </summary>
        IEnumerable<string> ExcludedTags { get; set; }

        /// <summary>
        /// Sets the least level of importance to log.
        /// </summary>
        CodeLogImportance MinimumImportance { get; set; }

        /// <summary>
        /// Log a message
        /// </summary>
        /// <param name="from">The place in the code logging the message</param>
        /// <param name="importance">The importance assigned to this message (for filtering)</param>
        /// <param name="tags">Any tags assigned to this message (for filtering)</param>
        /// <param name="dateTime">The time that should be logged</param>
        /// <param name="relAfter">A TimeSpan specifying time since logging for this piece of code started</param>
        /// <param name="Message">The message to log</param>
        void Log(string from, CodeLogImportance importance, IEnumerable<string> tags, DateTimeOffset dateTime, TimeSpan relAfter, string Message);

        /// <summary>
        /// Log an Exception
        /// </summary>
        /// <param name="from">The place in the code logging the message</param>
        /// <param name="tags">Any tags assigned to this message (for filtering)</param>
        /// <param name="dateTime">The time that should be logged</param>
        /// <param name="relAfter">A TimeSpan specifying time since logging for this piece of code started</param>
        /// <param name="exception">An Exception object to log</param>
        void LogException(string from, IEnumerable<string> tags, DateTimeOffset dateTime, TimeSpan relAfter, Exception exception);
    }
}
