﻿using JonRLevy.JRLUtilities.Collections.Generic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.JRLUtilities.Collections
{
    /// <summary>
    /// Adds extension-methods to IEnumerable and IEnumerable&lt;T&gt;
    /// </summary>
    public static class IEnumerableEx
    {
        /// <summary>
        /// Picks an item from the supplied enumerable every for every 'n' item. Starts with the first in the sequence.
        /// </summary>
        /// <param name="enumerable">The enumeration to pick from</param>
        /// <param name="n">The number of items per pick; ie, is it 3, the first item will be picked, two and three will be skipped,
        /// the fourth will be picked, etc.</param>
        /// <returns>The enumeration of the picked items</returns>
        public static IEnumerable EveryNth(this IEnumerable enumerable, int n)
        {
            if (n <= 0)
                yield break;

            ulong count = 0;
            foreach (object item in enumerable)
            {
                if (count++ % (ulong)n == 0)
                    yield return item;
            }
        }

        /// <summary>
        /// Picks an item from the supplied enumerable every for every 'n' item. Starts with the first in the sequence.
        /// </summary>
        /// <typeparam name="T">The type of items in the IEnumerable</typeparam>
        /// <param name="enumerable">The enumeration to pick from</param>
        /// <param name="n">The number of items per pick; ie, is it 3, the first item will be picked, two and three will be skipped,
        /// the fourth will be picked, etc.</param>
        /// <returns>The enumeration of the picked items</returns>
        public static IEnumerable<T> EveryNth<T>(this IEnumerable<T> enumerable, int n)
        {
            if (n <= 0)
                yield break;

            ulong count = 0;
            foreach (T item in enumerable)
            {
                if (count++ % (ulong)n == 0)
                    yield return item;
            }
        }

        /// <summary>
        /// This extension method returns a sample, by calling the Keep method of the supplied ISampler for each item.
        /// </summary>
        /// <typeparam name="T">The type of items in the IEnumerable</typeparam>
        /// <param name="enumerable">The IEnumerable containing items to sample</param>
        /// <param name="sampler">The sampler object, implementing the ISampler interface</param>
        /// <returns>The sample, consisting of items from the enumerable the sampler dertermined should be kept</returns>
        public static IEnumerable<T> Sample<T>(this IEnumerable<T> enumerable, ISampler<T> sampler)
        {
            foreach (T item in enumerable)
            {
                if (sampler.Keep(item))
                    yield return item;
            }
        }

        /// <summary>
        /// Returns a number of IEnumerable&lt;T&gt;s that are the total possible permutations of the IEnumerable&lt;T&gt;.
        /// Meant for convenience, not optimized for speed & space. And if it it to be traversed more than once, it should be
        /// made into a List or Array, so as not having to repeat the permutation logic.
        /// Permutes from left to right, so it goes:
        /// a b c
        /// a c b
        /// b a c
        /// b c a
        /// c a b
        /// c b a
        /// </summary>
        /// <typeparam name="T">The type of items in the IEnumerable</typeparam>
        /// <param name="enumerable">The IEnumerable containing items to permute</param>
        /// <returns>An IEnumerable of IEnumerable&lt;T&gt;'s - each being a permuation of the base IEnumerable&lt;T&gt;</returns>
        public static IEnumerable<IEnumerable<T>> Permutate<T>(this IEnumerable<T> enumerable)
        {
            int depth = enumerable.Count();

            if (depth == 1)
                yield return enumerable;

            if (depth <= 1)
                yield break;

            for (int baseIdx=0; baseIdx<depth; baseIdx++)
            {
                T baseItem = default(T);
                T[] sub = new T[depth - 1];
                int idx=0, subIdx = 0;

                foreach (T item in enumerable)
                {
                    if (idx == baseIdx)
                        baseItem = item;
                    else
                        sub[subIdx++] = item;

                    idx++;
                }

                foreach (IEnumerable<T> subPermutation in sub.Permutate())
                {
                    T[] resultPermutation = new T[depth];
                    int resultIdx = 0;

                    resultPermutation[resultIdx++] = baseItem;

                    foreach (T permutedItem in subPermutation)
                        resultPermutation[resultIdx++] = permutedItem;

                    yield return resultPermutation;
                }
            }
        }
    }
}
