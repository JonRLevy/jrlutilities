﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.JRLUtilities.Collections.Generic
{
    /// <summary>
    /// An interface that when implemented tells if a given sample should be kept or discarded.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISampler<T>
    {
        /// <summary>
        /// A method that determines if a sample should be kept or discarded
        /// </summary>
        /// <param name="sample">The sample to examine</param>
        /// <returns>Whether it should be kept (true) or discarded (false).</returns>
        bool Keep(T sample);
    }
}
