﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.JRLUtilities.Collections.Generic
{
    /// <summary>
    /// Implements a random ISampler; whether a sample should be kept or not is determined by random,
    /// based on values given in the constructor.
    /// </summary>
    /// <typeparam name="T">The type of the sample; as the Keep operation is random, the type is not really used.</typeparam>
    public class RandomSampler<T> : ISampler<T>
    {
        Random random;
        double sampleChance;
        
        private RandomSampler()
        {
            random = new Random();
            sampleChance = 0.0;
        }

        /// <summary>
        /// Creates an RandomSampler, the chance of a sample being kept is provided as a number between 0.0 and 1.0,
        /// where 1.0 is certain.
        /// </summary>
        /// <param name="sampleChance">The chance of a sample being kept, as a number between 0.0 and 1.0.</param>
        public RandomSampler(double sampleChance)
        {
            random = new Random();
            this.sampleChance = sampleChance;
        }

        /// <summary>
        /// Creates an RandomSampler, the chance of a sample being kept is provided as a number between 0.0 and 1.0,
        /// where 1.0 is certain.
        /// </summary>
        /// <param name="seed">A seed value for the internal randomizer; given the same seed and the same sampleChance,
        /// the order of keep/discard will be the same.</param>
        /// <param name="sampleChance">The chance of a sample being kept, as a number between 0.0 and 1.0.</param>
        public RandomSampler(int seed, double sampleChance)
        {
            random = new Random(seed);
            this.sampleChance = sampleChance;
        }

        /// <summary>
        /// Checks if a sample should be kept, the result of which is random, based on values given at construction. 
        /// </summary>
        /// <param name="sample">The sample to check; it's value is arbitrary, as it determined at random whether it should be kept.</param>
        /// <returns>Whether to keep (true) or discard (false) the sample.</returns>
        public bool Keep(T sample) => random.NextDouble() < sampleChance;

        /// <summary>
        /// Returns a Sample of the provided IEnumerable by calling the IEnumerableEx Sample-method.
        /// </summary>
        /// <param name="enumerable">The enumerable to make a random sample from</param>
        /// <returns>The random sample from the provided enumerable</returns>
        public IEnumerable<T> Sample(IEnumerable<T> enumerable) => enumerable.Sample(this);
    }
}
