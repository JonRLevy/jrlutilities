﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JonRLevy.JRLUtilities.Database
{
    /// <summary>
    /// This extension to the <c>SqlParameterCollection</c> adds dbtyped add-methods, that takes a nullable value-parameter.
    /// The advantage (compared to <c>AddWithValue</c>) is that you are sure which database-type is used, and if you provide
    /// a null-value, it is replaced with <c>DBNull</c>.
    /// </summary>
    public static class SqlParameterCollectionEx
    {
        /// <summary>
        /// Create and add a <c>Bit</c>-parameter, providing the value as a <c>bool?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddBit(this SqlParameterCollection parametercollection, String parameterName, bool? value)
            => addValue(parametercollection, parameterName, SqlDbType.Bit, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>TinyInt</c>-parameter, providing the value as a <c>byte?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddTinyInt(this SqlParameterCollection parametercollection, String parameterName, byte? value)
            => addValue(parametercollection, parameterName, SqlDbType.TinyInt, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>SmallInt</c>-parameter, providing the value as a <c>short?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddSmallInt(this SqlParameterCollection parametercollection, String parameterName, short? value)
            => addValue(parametercollection, parameterName, SqlDbType.SmallInt, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add an <c>Int</c>-parameter, providing the value as a <c>int?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddInt(this SqlParameterCollection parametercollection, String parameterName, int? value)
            => addValue(parametercollection, parameterName, SqlDbType.Int, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>BigInt</c>-parameter, providing the value as a <c>long?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddBigInt(this SqlParameterCollection parametercollection, String parameterName, long? value)
            => addValue(parametercollection, parameterName, SqlDbType.BigInt, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>Decimal</c>-parameter, providing the value as a <c>decimal?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddDecimal(this SqlParameterCollection parametercollection, String parameterName, decimal? value)
            => addValue(parametercollection, parameterName, SqlDbType.Decimal, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>Float</c>-parameter, providing the value as a <c>double?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddFloat(this SqlParameterCollection parametercollection, String parameterName, double? value)
            => addValue(parametercollection, parameterName, SqlDbType.Float, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>Real</c>-parameter, providing the value as a <c>float?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddReal(this SqlParameterCollection parametercollection, String parameterName, float? value)
            => addValue(parametercollection, parameterName, SqlDbType.Real, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>DateTimeOffset</c>-parameter, providing the value as a <c>DateTimeOffset?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddDateTimeOffset(this SqlParameterCollection parametercollection, String parameterName, DateTimeOffset? value)
            => addValue(parametercollection, parameterName, SqlDbType.DateTimeOffset, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>DateTime2</c>-parameter, providing the value as a <c>DateTime?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddDateTime2(this SqlParameterCollection parametercollection, String parameterName, DateTime? value)
            => addValue(parametercollection, parameterName, SqlDbType.DateTime2, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>DateTime</c>-parameter, providing the value as a <c>DateTime?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddDateTime(this SqlParameterCollection parametercollection, String parameterName, DateTime? value)
            => addValue(parametercollection, parameterName, SqlDbType.DateTime, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>SmallDateTime</c>-parameter, providing the value as a <c>DateTime?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddSmallDateTime(this SqlParameterCollection parametercollection, String parameterName, DateTime? value)
            => addValue(parametercollection, parameterName, SqlDbType.SmallDateTime, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>Time</c>-parameter, providing the value as a <c>TimeSpan?</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddTime(this SqlParameterCollection parametercollection, String parameterName, TimeSpan? value)
            => addValue(parametercollection, parameterName, SqlDbType.Time, value.HasValue ? (object)value.Value : DBNull.Value);

        /// <summary>
        /// Create and add an <c>NVarChar</c>-parameter, providing the value as a <c>string</c>. If the value is null, <c>DBNull</c> is added.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        public static SqlParameter AddNVarChar(this SqlParameterCollection parametercollection, String parameterName, string value)
            => addValue(parametercollection, parameterName, SqlDbType.NVarChar, value!=null ? (object)value : DBNull.Value);

        /// <summary>
        /// Create and add a <c>SqlParameter</c> of the specified DB-type, give it the specified value, and add it to the collection.
        /// </summary>
        /// <param name="parametercollection">The collection to add to.</param>
        /// <param name="parameterName">The name of the parameter to create.</param>
        /// <param name="sqlDbType">The database-type of the parameter to create.</param>
        /// <param name="value">The value to set the parameter to. In case of <c>null</c>, <c>DBNull</c> is used.</param>
        /// <returns>The created and added parameter.</returns>
        private static SqlParameter addValue(this SqlParameterCollection parametercollection, String parameterName, SqlDbType sqlDbType, Object value)
        {
            SqlParameter p = new SqlParameter(parameterName, sqlDbType);
            p.Value = value;
            parametercollection.Add(p);

            return p;
        }
    }
}
