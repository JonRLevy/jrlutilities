﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using JonRLevy.JRLUtilities.Tasks;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class AsyncsTests
    {
        List<int> getTestList0()
        {
            List<int> l = new List<int>();

            return l;
        }

        List<int> getTestList1()
        {
            List<int> l = new List<int>();
            l.Add(3);

            return l;
        }

        List<int> getTestList3()
        {
            List<int> l = new List<int>();
            l.Add(3);
            l.Add(5);
            l.Add(8);

            return l;
        }

        [TestMethod]
        public async Task ForEachGetPutAsyncTest()
        {
            int result1 = await Asyncs.ForEachGetPutAsync<int, string>
            (
                getTestList0(),
                async x => 
                {
                    await Task.Delay(1);
                    return x.ToString();
                },
                async x => 
                {
                    await Task.Delay(1);
                }
            );
            Assert.AreEqual(0, result1);

            int result2 = await Asyncs.ForEachGetPutAsync<int, string>
            (
                getTestList1(),
                async x =>
                {
                    await Task.Delay(1);
                    return x.ToString();
                },
                async x =>
                {
                    await Task.Delay(1);
                }
            );
            Assert.AreEqual(1, result2);

            int result3 = await Asyncs.ForEachGetPutAsync<int, string>
            (
                getTestList3(),
                async x =>
                {
                    await Task.Delay(1);
                    return x.ToString();
                },
                async x =>
                {
                    await Task.Delay(1);
                }
            );
            Assert.AreEqual(3, result3);

        }

        [TestMethod]
        public async Task ForEachGetPutAsyncWithContextsTest()
        {
            int result1 = await Asyncs.ForEachGetPutAsync<int, string, int, int>
            (
                getTestList0(),
                2,
                4,
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    return (x*ctxt).ToString();
                },
                async (ctxt, y) =>
                {
                    await Task.Delay(1);
                }
            );
            Assert.AreEqual(0, result1);

            int result2 = await Asyncs.ForEachGetPutAsync<int, string, int, int>
            (
                getTestList1(),
                2,
                4,
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    return (x * ctxt).ToString();
                },
                async (ctxt, y) =>
                {
                    await Task.Delay(1);
                }
            );
            Assert.AreEqual(1, result2);

            int result3 = await Asyncs.ForEachGetPutAsync<int, string, int, int>
            (
                getTestList3(),
                2,
                4,
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    return (x * ctxt).ToString();
                },
                async (ctxt, y) =>
                {
                    await Task.Delay(1);
                }
            );
            Assert.AreEqual(3, result3);
        }

        [TestMethod]
        public async Task ForEachGetPutAsyncWithContextsAndReturnTest()
        {
            List<byte> result1 = await Asyncs.ForEachGetPutAsync<int, string, byte, int, int>
            (
                getTestList0(),
                2,
                4,
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    return (x * ctxt).ToString();
                },
                async (ctxt, y) =>
                {
                    await Task.Delay(1);
                    return byte.Parse(y + ctxt.ToString());
                }
            );
            Assert.AreEqual(0, result1.Count);

            List<byte> result2 = await Asyncs.ForEachGetPutAsync<int, string, byte, int, int>
            (
                getTestList1(),
                2,
                4,
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    return (x * ctxt).ToString();
                },
                async (ctxt, y) =>
                {
                    await Task.Delay(1);
                    return byte.Parse(y + ctxt.ToString());
                }
            );
            Assert.AreEqual(1, result2.Count);
            Assert.AreEqual(64, result2[0]);

            List<byte> result3 = await Asyncs.ForEachGetPutAsync<int, string, byte, int, int>
            (
                getTestList3(),
                2,
                4,
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    return (x * ctxt).ToString();
                },
                async (ctxt, y) =>
                {
                    await Task.Delay(1);
                    return byte.Parse(y + ctxt.ToString());
                }
            );
            Assert.AreEqual(3, result3.Count);
            Assert.AreEqual(64, result3[0]);
            Assert.AreEqual(104, result3[1]);
            Assert.AreEqual(164, result3[2]);
        }


        [TestMethod]
        public async Task ParallelForEachAsyncTest()
        {
            await Asyncs.ParallelForEachAsync<int>
            (
                getTestList3(),
                x => { x.ToString(); }
            );
        }

        [TestMethod]
        public async Task ParallelForAsyncTest()
        {
            await Asyncs.ParallelForAsync
            (
                0, 3,
                x => { x.ToString(); }
            );
        }

        [TestMethod]
        public async Task ForEachAsyncTest()
        {
            await Asyncs.ForEachAsync<int>
            (
                getTestList0(),
                async x =>
                {
                    await Task.Delay(1);
                },
                2
            );

            await Asyncs.ForEachAsync<int>
            (
                getTestList1(),
                async x =>
                {
                    await Task.Delay(1);
                },
                2
            );

            await Asyncs.ForEachAsync<int>
            (
                getTestList3(),
                async x =>
                {
                    await Task.Delay(1);
                },
                2
            );
        }

        [TestMethod]
        public async Task ForEachAsyncWithContextTest()
        {
            await Asyncs.ForEachAsync<int, string>
            (
                "hello",
                getTestList0(),
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    string y = ctxt + x.ToString();
                },
                2
            );

            await Asyncs.ForEachAsync<int, string>
            (
                "hello",
                getTestList1(),
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    string y = ctxt + x.ToString();
                },
                2
            );

            await Asyncs.ForEachAsync<int, string>
            (
                "hello",
                getTestList3(),
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    string y = ctxt + x.ToString();
                },
                2
            );

        }

        [TestMethod]
        public async Task ForEachAsyncWithContextAndReturnTest()
        {
            Tuple<int, string>[] result1 = await Asyncs.ForEachAsync<int, string, string>
            (
                "hello",
                getTestList0(),
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    return ctxt + x.ToString();
                },
                2
            );
            Assert.AreEqual(0, result1.Length);

            Tuple<int, string>[] result2 = await Asyncs.ForEachAsync<int, string, string>
            (
                "hello",
                getTestList1(),
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    return ctxt + x.ToString();
                },
                2
            );
            Assert.AreEqual(1, result2.Length);
            Assert.AreEqual(3, result2[0].Item1);
            Assert.AreEqual("hello3", result2[0].Item2);

            Tuple<int, string>[] result3 = await Asyncs.ForEachAsync<int, string, string>
            (
                "hello",
                getTestList3(),
                async (ctxt, x) =>
                {
                    await Task.Delay(1);
                    return ctxt + x.ToString();
                },
                2
            );
            Assert.AreEqual(3, result3.Length);
            Assert.AreEqual(3, result3[0].Item1);
            Assert.AreEqual("hello3", result3[0].Item2);
            Assert.AreEqual(5, result3[1].Item1);
            Assert.AreEqual("hello5", result3[1].Item2);
            Assert.AreEqual(8, result3[2].Item1);
            Assert.AreEqual("hello8", result3[2].Item2);
        }
    }
}
