﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using JonRLevy.JRLUtilities.Collections.Generic;
using JonRLevy.JRLUtilities.Cache;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class CacheTests
    {
        Dictionary<int, Object> testdb;
        Object obj1, obj2, obj3, obj4, obj5; 
        public CacheTests()
        {
            testdb = new Dictionary<int, object>();
            obj1 = new object();
            obj2 = new object();
            obj3 = new object();
            obj4 = new object();
            obj5 = new object();

            testdb[1] = obj1;
            testdb[2] = obj2;
            testdb[3] = obj3;
            testdb[4] = obj4;
            testdb[5] = obj5;
        }

        public async Task<Object> GetFromDB(Dictionary<int, Object> DB, int Key)
        {
            await Task.Delay(50);
            Object result = null;
            DB.TryGetValue(Key, out result);

            return result;
        }

        [TestMethod]
        public async Task GetSingleCacheValueTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();

            Object result1 = await cache.GetAsync(testdb, 2, GetFromDB);
            Assert.AreEqual(obj2, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0.0, cache.HitMissRatio, "Unexpected HitRate after Get");
        }

        [TestMethod]
        public async Task GetCacheValuesWithShrinkageLatestRequestTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.MaxItems = 4;
            cache.ShrinkTo = 2;
            cache.DefaultFetchRequestAsync = GetFromDB;
            cache.ShrinkageScheme = CacheShrinkageScheme.LatestRequest;

            Object result1 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0.0, cache.HitMissRatio, "Unexpected HitRate after Get");

            Object result2 = await cache.GetAsync(testdb, 3);
            await Task.Delay(50);
            Assert.AreEqual(obj3, result2, "Unexpected item returned from Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0.0, cache.HitMissRatio, "Unexpected HitRate after Get");

            Object result3 = await cache.GetAsync(testdb, 4);
            await Task.Delay(50);
            Assert.AreEqual(obj4, result3, "Unexpected item returned from Get");
            Assert.AreEqual(3, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0.0, cache.HitMissRatio, "Unexpected HitRate after Get");

            Object result4 = await cache.GetAsync(testdb, 5);
            await Task.Delay(50);
            Assert.AreEqual(obj5, result4, "Unexpected item returned from Get");
            Assert.AreEqual(4, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0.0, cache.HitMissRatio, "Unexpected HitRate after Get");

            Object result5 = await cache.GetAsync(testdb, 3);
            await Task.Delay(50);
            Assert.AreEqual(obj3, result5, "Unexpected item returned from Get");
            Assert.AreEqual(4, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0.25, cache.HitMissRatio, "Unexpected HitRate after Get");

            Object result6 = await cache.GetAsync(testdb, 1);
            await Task.Delay(50);
            Assert.AreEqual(obj1, result6, "Unexpected item returned from Get");
            Assert.AreEqual(0.2, cache.HitMissRatio, "Unexpected HitRate after Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result7 = await cache.GetAsync(testdb, 3);
            await Task.Delay(50);
            Assert.AreEqual(obj3, result7, "Unexpected item returned from Get");
            Assert.AreEqual(0.4, cache.HitMissRatio, "Unexpected HitRate after Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result8 = await cache.GetAsync(testdb, 1);
            await Task.Delay(50);
            Assert.AreEqual(obj1, result8, "Unexpected item returned from Get");
            Assert.AreEqual(0.6, cache.HitMissRatio, "Unexpected HitRate after Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");
        }

        [TestMethod]
        public async Task GetCacheValuesWithStalenessTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.DefaultFetchRequestAsync = GetFromDB;
            cache.StaleAfter = new TimeSpan(0, 0, 0, 0, 200);

            Object result1 = await cache.GetAsync(testdb, 2);
            await Task.Delay(20);
            Assert.AreEqual(obj2, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0.0, cache.HitMissRatio, "Unexpected HitRate after Get");

            Object result2 = await cache.GetAsync(testdb, 2);
            await Task.Delay(500);
            Assert.AreEqual(obj2, result2, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(1, cache.HitMissRatio, "Unexpected HitRate after Get");

            Object result3 = await cache.GetAsync(testdb, 2);
            await Task.Delay(20);
            Assert.AreEqual(obj2, result2, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0.5, cache.HitMissRatio, "Unexpected HitRate after Get");
        }

        [TestMethod]
        public async Task GetCacheValuesWithStaleShrinkageTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.DefaultFetchRequestAsync = GetFromDB;
            cache.StaleAfter = new TimeSpan(0, 0, 0, 0, 200);

            Object result1 = await cache.GetAsync(testdb, 2);
            await Task.Delay(20);
            Assert.AreEqual(obj2, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            await Task.Delay(300);
            cache.ShrinkAsync();
            await Task.Delay(100);
            Assert.AreEqual(0, cache.CurrentItemCount, "Unexpected ItemCount after Shrink with staleness");
        }

        [TestMethod]
        public async Task GetCacheValuesWithNoStaleShrinkageTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.DefaultFetchRequestAsync = GetFromDB;

            Object result1 = await cache.GetAsync(testdb, 2);
            await Task.Delay(20);
            Assert.AreEqual(obj2, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            await Task.Delay(300);
            cache.ShrinkAsync();
            await Task.Delay(100);
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Shrink with no staleness");
        }

        [TestMethod]
        public async Task GetNullValueWithStoreNullTrueTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.DefaultFetchRequestAsync = GetFromDB;
            cache.StoreNull = true;

            Object result1 = await cache.GetAsync(testdb, 6);
            await Task.Delay(20);
            Assert.AreEqual(null, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0, cache.HitMissRatio, "Unexpected HitRate after Get");

            Object result2 = await cache.GetAsync(testdb, 6);
            await Task.Delay(20);
            Assert.AreEqual(null, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(1, cache.HitMissRatio, "Unexpected HitRate after Get");
        }

        [TestMethod]
        public async Task GetNullValueWithStoreNullFalseTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.DefaultFetchRequestAsync = GetFromDB;
            cache.StoreNull = false;

            Object result1 = await cache.GetAsync(testdb, 6);
            await Task.Delay(20);
            Assert.AreEqual(null, result1, "Unexpected item returned from Get");
            Assert.AreEqual(0, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0, cache.HitMissRatio, "Unexpected HitRate after Get");

            Object result2 = await cache.GetAsync(testdb, 6);
            await Task.Delay(20);
            Assert.AreEqual(null, result1, "Unexpected item returned from Get");
            Assert.AreEqual(0, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0, cache.HitMissRatio, "Unexpected HitRate after Get");
        }

        [TestMethod]
        public async Task GetNullValueWithStoreNullChangeTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.DefaultFetchRequestAsync = GetFromDB;
            cache.StoreNull = true;

            Object result1 = await cache.GetAsync(testdb, 6);
            await Task.Delay(20);
            Assert.AreEqual(null, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
            Assert.AreEqual(0, cache.HitMissRatio, "Unexpected HitRate after Get");

            cache.StoreNull = false;
            Assert.AreEqual(0, cache.CurrentItemCount, "Unexpected ItemCount after StoreNull=false");
        }

        [TestMethod]
        public async Task GetCacheValuesWithShrinkageRequestCountTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.MaxItems = 2;
            cache.ShrinkTo = 1;
            cache.DefaultFetchRequestAsync = GetFromDB;
            cache.ShrinkageScheme = CacheShrinkageScheme.RequestCount;

            Object result1 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result2 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result2, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result3 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result3, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result4 = await cache.GetAsync(testdb, 3);
            await Task.Delay(50);
            Assert.AreEqual(obj3, result4, "Unexpected item returned from Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result5 = await cache.GetAsync(testdb, 4);
            await Task.Delay(50);
            Assert.AreEqual(obj4, result5, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result6 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result6, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
        }

        [TestMethod]
        public async Task GetCacheValuesWithShrinkageRequestsPerMinuteTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.MaxItems = 2;
            cache.ShrinkTo = 2;
            cache.DefaultFetchRequestAsync = GetFromDB;
            cache.ShrinkageScheme = CacheShrinkageScheme.RequestsPerMinute;

            Object result1 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result2 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result2, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result3 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result3, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result4 = await cache.GetAsync(testdb, 3);
            await Task.Delay(1000);
            Assert.AreEqual(obj3, result4, "Unexpected item returned from Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result5 = await cache.GetAsync(testdb, 4);
            await Task.Delay(50);
            Assert.AreEqual(obj4, result5, "Unexpected item returned from Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result6 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result6, "Unexpected item returned from Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result7 = await cache.GetAsync(testdb, 4);
            await Task.Delay(50);
            Assert.AreEqual(obj4, result7, "Unexpected item returned from Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");
        }

        [TestMethod]
        public async Task GetCacheValuesWithShrinkageRandomTest()
        {
            Cache<int, Object, Dictionary<int, Object>> cache = new Cache<int, Object, Dictionary<int, Object>>();
            cache.MaxItems = 2;
            cache.ShrinkTo = 1;
            cache.DefaultFetchRequestAsync = GetFromDB;
            cache.ShrinkageScheme = CacheShrinkageScheme.Random;

            Object result1 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result1, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result2 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result2, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result3 = await cache.GetAsync(testdb, 2);
            await Task.Delay(50);
            Assert.AreEqual(obj2, result3, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result4 = await cache.GetAsync(testdb, 3);
            await Task.Delay(50);
            Assert.AreEqual(obj3, result4, "Unexpected item returned from Get");
            Assert.AreEqual(2, cache.CurrentItemCount, "Unexpected ItemCount after Get");

            Object result5 = await cache.GetAsync(testdb, 4);
            await Task.Delay(50);
            Assert.AreEqual(obj4, result5, "Unexpected item returned from Get");
            Assert.AreEqual(1, cache.CurrentItemCount, "Unexpected ItemCount after Get");
        }
    }
}
