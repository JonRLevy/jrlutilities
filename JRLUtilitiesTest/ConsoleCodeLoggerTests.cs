﻿using JonRLevy.JRLUtilities.IO;
using JonRLevy.JRLUtilities.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class ConsoleCodeLoggerTests
    {
        [TestMethod]
        public void ConsoleCodeLoggerLogEntryExitTest()
        {
            ConsoleCodeLogger codeLogger = new ConsoleCodeLogger();
            string caught;
            using (ConsoleOutputStringCatcher stringCatcher = new ConsoleOutputStringCatcher())
            {
                using (CodeLoggerItem codeLoggerItem = new CodeLoggerItem(codeLogger))
                {

                }
                caught = stringCatcher.GetStringOuput();
            }
            bool found1 = (caught.IndexOf("ConsoleCodeLoggerLogEntryExitTest after") >= 0);
            Assert.AreEqual(true, found1);

            bool found2 = (caught.IndexOf("Entering") >= 0);
            Assert.AreEqual(true, found2);

            bool found3 = (caught.IndexOf("Exiting") >= 0);
            Assert.AreEqual(true, found3);
        }

        [TestMethod]
        public void ConsoleCodeLoggerLogMessageTest()
        {
            ConsoleCodeLogger codeLogger = new ConsoleCodeLogger();
            string caught;
            using (ConsoleOutputStringCatcher stringCatcher = new ConsoleOutputStringCatcher())
            {
                using (CodeLoggerItem codeLoggerItem = new CodeLoggerItem(codeLogger))
                {
                    codeLoggerItem.Log("This is a test message");
                }
                caught = stringCatcher.GetStringOuput();
            }
            bool found1 = (caught.IndexOf("ConsoleCodeLoggerLogMessageTest after") >= 0);
            Assert.AreEqual(true, found1);

            bool found2 = (caught.IndexOf("This is a test message") >= 0);
            Assert.AreEqual(true, found2);
        }

        [TestMethod]
        public void ConsoleCodeLoggerLogExceptionTest()
        {
            ConsoleCodeLogger codeLogger = new ConsoleCodeLogger();
            string caught;
            using (ConsoleOutputStringCatcher stringCatcher = new ConsoleOutputStringCatcher())
            {
                using (CodeLoggerItem codeLoggerItem = new CodeLoggerItem(codeLogger))
                {
                    try
                    {
                        int a = 10;
                        int b = 20;
                        int c = b / (a - 10);
                    }
                    catch (Exception ex)
                    {
                        Exception ex2=codeLoggerItem.LogException(ex);
                        Assert.AreSame(ex, ex2);
                    }
                }
                caught = stringCatcher.GetStringOuput();
            }
            bool found1 = (caught.IndexOf("ConsoleCodeLoggerLogExceptionTest after") >= 0);
            Assert.AreEqual(true, found1);

            bool found2 = (caught.IndexOf("DivideByZeroException") >= 0);
            Assert.AreEqual(true, found2);
        }

        [TestMethod]
        public void ConsoleCodeLoggerLowImportanceTest()
        {
            ConsoleCodeLogger codeLogger = new ConsoleCodeLogger(CodeLogImportance.Warning);
            string caught;
            using (ConsoleOutputStringCatcher stringCatcher = new ConsoleOutputStringCatcher())
            {
                using (CodeLoggerItem codeLoggerItem = new CodeLoggerItem(codeLogger))
                {

                }
                caught = stringCatcher.GetStringOuput();
            }
            bool found1 = (caught.IndexOf("ConsoleCodeLoggerLogEntryExitTest after") >= 0);
            Assert.AreEqual(false, found1);

            bool found2 = (caught.IndexOf("Entering") >= 0);
            Assert.AreEqual(false, found2);

            bool found3 = (caught.IndexOf("Exiting") >= 0);
            Assert.AreEqual(false, found3);
        }

        [TestMethod]
        public void ConsoleCodeLoggerLogImportanceMessageTest()
        {
            ConsoleCodeLogger codeLogger = new ConsoleCodeLogger(CodeLogImportance.Warning);
            string caught;
            using (ConsoleOutputStringCatcher stringCatcher = new ConsoleOutputStringCatcher())
            {
                using (CodeLoggerItem codeLoggerItem = new CodeLoggerItem(codeLogger))
                {
                    codeLoggerItem.Log("This is a test notification", CodeLogImportance.Notification);
                    codeLoggerItem.Log("This is a test warning", CodeLogImportance.Warning);
                }
                caught = stringCatcher.GetStringOuput();
            }
            bool found1 = (caught.IndexOf("This is a test notification") >= 0);
            Assert.AreEqual(false, found1);

            bool found2 = (caught.IndexOf("This is a test warning") >= 0);
            Assert.AreEqual(true, found2);
        }

        [TestMethod]
        public void ConsoleCodeLoggerTagMissingTest()
        {
            ConsoleCodeLogger codeLogger = new ConsoleCodeLogger(CodeLogImportance.Notification, new List<string>{ "testtag1" } );
            string caught;
            using (ConsoleOutputStringCatcher stringCatcher = new ConsoleOutputStringCatcher())
            {
                using (CodeLoggerItem codeLoggerItem = new CodeLoggerItem(codeLogger))
                {

                }
                caught = stringCatcher.GetStringOuput();
            }
            bool found1 = (caught.IndexOf("ConsoleCodeLoggerLogEntryExitTest after") >= 0);
            Assert.AreEqual(false, found1);

            bool found2 = (caught.IndexOf("Entering") >= 0);
            Assert.AreEqual(false, found2);

            bool found3 = (caught.IndexOf("Exiting") >= 0);
            Assert.AreEqual(false, found3);
        }

        [TestMethod]
        public void ConsoleCodeLoggerTagIncludedTest()
        {
            ConsoleCodeLogger codeLogger = new ConsoleCodeLogger(CodeLogImportance.Notification, new List<string> { "testtag1" });
            string caught;
            using (ConsoleOutputStringCatcher stringCatcher = new ConsoleOutputStringCatcher())
            {
                using (CodeLoggerItem codeLoggerItem = new CodeLoggerItem(codeLogger))
                {
                    codeLoggerItem.Log("This is a test notification", CodeLogImportance.Notification, new List<string> { "testtag1" } );
                    codeLoggerItem.Log("This is a test warning", CodeLogImportance.Warning);
                }
                caught = stringCatcher.GetStringOuput();
            }

            bool found1 = (caught.IndexOf("This is a test notification") >= 0);
            Assert.AreEqual(true, found1);

            bool found2 = (caught.IndexOf("This is a test warning") >= 0);
            Assert.AreEqual(false, found2);
        }

        [TestMethod]
        public void ConsoleCodeLoggerTagExcludedTest()
        {
            ConsoleCodeLogger codeLogger = new ConsoleCodeLogger(CodeLogImportance.Notification, null, new List<string> { "testtag1" });
            string caught;
            using (ConsoleOutputStringCatcher stringCatcher = new ConsoleOutputStringCatcher())
            {
                using (CodeLoggerItem codeLoggerItem = new CodeLoggerItem(codeLogger))
                {
                    codeLoggerItem.Log("This is a test notification", CodeLogImportance.Notification, new List<string> { "testtag1" });
                    codeLoggerItem.Log("This is a test warning", CodeLogImportance.Warning);
                }
                caught = stringCatcher.GetStringOuput();
            }

            bool found1 = (caught.IndexOf("This is a test notification") >= 0);
            Assert.AreEqual(false, found1);

            bool found2 = (caught.IndexOf("This is a test warning") >= 0);
            Assert.AreEqual(true, found2);
        }
    }
}
