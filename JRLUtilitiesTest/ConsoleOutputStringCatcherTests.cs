﻿using JonRLevy.JRLUtilities.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class ConsoleOutputStringCatcherTests
    {
        [TestMethod]
        public void ConsoleOutputStringCatchTest()
        {
            Console.WriteLine("Should go to regular output.");
            string result1;

            using (ConsoleOutputStringCatcher outputStringCatcher = new ConsoleOutputStringCatcher())
            {
                Console.WriteLine("Should be caught by catcher.");
                result1 = outputStringCatcher.GetStringOuput();
            }
            Console.WriteLine("Should go back to regular output.");

            Assert.AreEqual("Should be caught by catcher.\r\n", result1);
        }

        [TestMethod]
        public void ConsoleOutputStringCatchClearTest()
        {
            string result1;

            using (ConsoleOutputStringCatcher outputStringCatcher = new ConsoleOutputStringCatcher())
            {
                Console.WriteLine("Should be caught by catcher.");
                outputStringCatcher.ClearOutput();
                Console.WriteLine("Should be the only line in the catcher.");

                result1 = outputStringCatcher.GetStringOuput();
            }

            Assert.AreEqual("Should be the only line in the catcher.\r\n", result1);
        }
    }
}
