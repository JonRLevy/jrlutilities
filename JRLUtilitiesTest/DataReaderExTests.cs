﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JonRLevy.JRLUtilities.Database;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class DataReaderExTests
    {
        MockDataReader mdr = new MockDataReader();

        [TestMethod]
        public void TestDataReaderExGetBool()
        {
            bool b1 = mdr.GetBoolean(0, false);
            Assert.AreEqual(true, b1, "Unexpected return from GetBoolean(0, false)");

            bool b2 = mdr.GetBoolean(1, false);
            Assert.AreEqual(false, b2, "Unexpected return from GetBoolean(1, false)");

            bool? nb1 = mdr.GetNullableBoolean(0);
            Assert.AreEqual((bool?)true, nb1, "Unexpected return from GetNullableBoolean(0)");

            bool? nb2 = mdr.GetNullableBoolean(1);
            Assert.AreEqual((bool?)null, nb2, "Unexpected return from GetNullableBoolean(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetByte()
        {
            byte b1 = mdr.GetByte(0, 0);
            Assert.AreEqual(byte.MaxValue, b1, "Unexpected return from GetByte(0, 0)");

            byte b2 = mdr.GetByte(1, 0);
            Assert.AreEqual(0, b2, "Unexpected return from GetByte(1, 0)");

            byte? nb1 = mdr.GetNullableByte(0);
            Assert.AreEqual((byte?)byte.MaxValue, nb1, "Unexpected return from GetNullableByte(0)");

            byte? nb2 = mdr.GetNullableByte(1);
            Assert.AreEqual((byte?)null, nb2, "Unexpected return from GetNullableByte(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetChar()
        {
            Char c1 = mdr.GetChar(0, 'a');
            Assert.AreEqual(Char.MaxValue, c1, "Unexpected return from GetChar(0, 'a')");

            Char c2 = mdr.GetChar(1, 'a');
            Assert.AreEqual('a', c2, "Unexpected return from GetChar(1, 'a')");

            Char? nc1 = mdr.GetNullableChar(0);
            Assert.AreEqual((Char?)Char.MaxValue, nc1, "Unexpected return from GetNullableChar(0)");

            Char? nc2 = mdr.GetNullableChar(1);
            Assert.AreEqual((Char?)null, nc2, "Unexpected return from GetNullableChar(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetInt16()
        {
            Int16 i1 = mdr.GetInt16(0, 0);
            Assert.AreEqual(Int16.MaxValue, i1, "Unexpected return from GetInt16(0, 0)");

            Int16 i2 = mdr.GetInt16(1, 0);
            Assert.AreEqual(0, i2, "Unexpected return from GetInt16(1, 0)");

            Int16? ni1 = mdr.GetNullableInt16(0);
            Assert.AreEqual((Int16?)Int16.MaxValue, ni1, "Unexpected return from GetNullableInt16(0)");

            Int16? ni2 = mdr.GetNullableInt16(1);
            Assert.AreEqual((Int16?)null, ni2, "Unexpected return from GetNullableInt16(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetInt32()
        {
            Int32 i1 = mdr.GetInt32(0, 0);
            Assert.AreEqual(Int32.MaxValue, i1, "Unexpected return from GetInt32(0, 0)");

            Int32 i2 = mdr.GetInt32(1, 0);
            Assert.AreEqual(0, i2, "Unexpected return from GetInt32(1, 0)");

            Int32? ni1 = mdr.GetNullableInt32(0);
            Assert.AreEqual((Int32?)Int32.MaxValue, ni1, "Unexpected return from GetNullableInt32(0)");

            Int32? ni2 = mdr.GetNullableInt32(1);
            Assert.AreEqual((Int32?)null, ni2, "Unexpected return from GetNullableInt32(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetInt64()
        {
            Int64 i1 = mdr.GetInt64(0, 0);
            Assert.AreEqual(Int64.MaxValue, i1, "Unexpected return from GetInt64(0, 0)");

            Int64 i2 = mdr.GetInt64(1, 0);
            Assert.AreEqual(0, i2, "Unexpected return from GetInt64(1, 0)");

            Int64? ni1 = mdr.GetNullableInt64(0);
            Assert.AreEqual((Int64?)Int64.MaxValue, ni1, "Unexpected return from GetNullableInt64(0)");

            Int64? ni2 = mdr.GetNullableInt64(1);
            Assert.AreEqual((Int64?)null, ni2, "Unexpected return from GetNullableInt64(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetDecimal()
        {
            Decimal d1 = mdr.GetDecimal(0, 0);
            Assert.AreEqual(Decimal.MaxValue, d1, "Unexpected return from GetDecimal(0, 0)");

            Decimal d2 = mdr.GetDecimal(1, 0);
            Assert.AreEqual(0, d2, "Unexpected return from GetDecimal(1, 0)");

            Decimal? nd1 = mdr.GetNullableDecimal(0);
            Assert.AreEqual((Decimal?)Decimal.MaxValue, nd1, "Unexpected return from GetNullableDecimal(0)");

            Decimal? nd2 = mdr.GetNullableDecimal(1);
            Assert.AreEqual((Decimal?)null, nd2, "Unexpected return from GetNullableDecimal(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetDouble()
        {
            Double d1 = mdr.GetDouble(0, 0);
            Assert.AreEqual(Double.MaxValue, d1, "Unexpected return from GetDouble(0, 0)");

            Double d2 = mdr.GetDouble(1, 0);
            Assert.AreEqual(0, d2, "Unexpected return from GetDouble(1, 0)");

            Double? nd1 = mdr.GetNullableDouble(0);
            Assert.AreEqual((Double?)Double.MaxValue, nd1, "Unexpected return from GetNullableDouble(0)");

            Double? nd2 = mdr.GetNullableDouble(1);
            Assert.AreEqual((Double?)null, nd2, "Unexpected return from GetNullableDouble(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetFloat()
        {
            Single f1 = mdr.GetFloat(0, 0);
            Assert.AreEqual(Single.MaxValue, f1, "Unexpected return from GetFloat(0, 0)");

            Single f2 = mdr.GetFloat(1, 0);
            Assert.AreEqual(0, f2, "Unexpected return from GetFloat(1, 0)");

            Single? nf1 = mdr.GetNullableFloat(0);
            Assert.AreEqual((Single?)Single.MaxValue, nf1, "Unexpected return from GetNullableFloat(0)");

            Single? nf2 = mdr.GetNullableFloat(1);
            Assert.AreEqual((Single?)null, nf2, "Unexpected return from GetNullableFloat(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetDateTime()
        {
            DateTime testNow = DateTime.Now;
            DateTime d1 = mdr.GetDateTime(0, testNow);
            Assert.AreEqual(DateTime.MaxValue, d1, "Unexpected return from GetDateTime(0, 0)");

            DateTime d2 = mdr.GetDateTime(1, testNow);
            Assert.AreEqual(testNow, d2, "Unexpected return from GetDateTime(1, 0)");

            DateTime? nd1 = mdr.GetNullableDateTime(0);
            Assert.AreEqual((DateTime?)DateTime.MaxValue, nd1, "Unexpected return from GetNullableDateTime(0)");

            DateTime? nd2 = mdr.GetNullableDateTime(1);
            Assert.AreEqual((DateTime?)null, nd2, "Unexpected return from GetNullableDateTime(1)");
        }

        [TestMethod]
        public void TestDataReaderExGetValue()
        {
            Object testObj = new object();
            Object o1 = mdr.GetValue(0, testObj);
            Assert.AreSame(mdr.TestObject, o1, "Unexpected return from GetValue");

            Object o2 = mdr.GetValue(1, testObj);
            Assert.AreSame(testObj, o2, "Unexpected return from GetValue");
        }
    }

    class MockDataReader : IDataReader
    {
        public object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public object this[int i]
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int Depth
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int FieldCount
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsClosed
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public int RecordsAffected
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Close()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool GetBoolean(int i)
        {
            return true;
        }

        public byte GetByte(int i)
        {
            return Byte.MaxValue;
        }

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public char GetChar(int i)
        {
            return Char.MaxValue;
        }

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public IDataReader GetData(int i)
        {
            throw new NotImplementedException();
        }

        public string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public DateTime GetDateTime(int i)
        {
            return DateTime.MaxValue;
        }

        public decimal GetDecimal(int i)
        {
            return Decimal.MaxValue;
        }

        public double GetDouble(int i)
        {
            return Double.MaxValue;
        }

        public Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public float GetFloat(int i)
        {
            return Single.MaxValue;
        }

        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public short GetInt16(int i)
        {
            return Int16.MaxValue;
        }

        public int GetInt32(int i)
        {
            return Int32.MaxValue;
        }

        public long GetInt64(int i)
        {
            return Int64.MaxValue;
        }

        public string GetName(int i)
        {
            throw new NotImplementedException();
        }

        public int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        public DataTable GetSchemaTable()
        {
            throw new NotImplementedException();
        }

        public string GetString(int i)
        {
            return "Hello";
        }

        public object TestObject = new object();
        public object GetValue(int i)
        {
            return TestObject;
        }

        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public bool IsDBNull(int i)
        {
            if (i == 0)
                return false;
            else
                return true;
        }

        public bool NextResult()
        {
            throw new NotImplementedException();
        }

        public bool Read()
        {
            throw new NotImplementedException();
        }
    }

}
