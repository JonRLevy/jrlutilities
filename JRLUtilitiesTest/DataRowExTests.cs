﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JonRLevy.JRLUtilities.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class DataRowExTests
    {
        private DataTable makeTestTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("string", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("bool", Type.GetType("System.Boolean")));
            dt.Columns.Add(new DataColumn("byte", Type.GetType("System.Byte")));
            dt.Columns.Add(new DataColumn("short", Type.GetType("System.Int16")));
            dt.Columns.Add(new DataColumn("int", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("long", Type.GetType("System.Int64")));
            dt.Columns.Add(new DataColumn("decimal", Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("double", Type.GetType("System.Double")));
            dt.Columns.Add(new DataColumn("float", Type.GetType("System.Single")));
            dt.Columns.Add(new DataColumn("datetimeoffset", Type.GetType("System.DateTimeOffset")));
            dt.Columns.Add(new DataColumn("datetime", Type.GetType("System.DateTime")));
            dt.Columns.Add(new DataColumn("timespan", Type.GetType("System.TimeSpan")));

            DataRow values = dt.NewRow();
            values["string"] = "Hello";
            values["bool"] = true;
            values["byte"] = byte.MaxValue;
            values["short"] = short.MaxValue;
            values["int"] = int.MaxValue;
            values["long"] = long.MaxValue;
            values["decimal"] = decimal.MaxValue;
            values["double"] = double.MaxValue;
            values["float"] = float.MaxValue;
            values["datetimeoffset"] = DateTimeOffset.MaxValue;
            values["datetime"] = DateTime.MaxValue;
            values["timespan"] = TimeSpan.MaxValue;
            dt.Rows.Add(values);

            DataRow nulls = dt.NewRow();
            nulls["string"] = DBNull.Value;
            nulls["bool"] = DBNull.Value;
            nulls["byte"] = DBNull.Value;
            nulls["short"] = DBNull.Value;
            nulls["int"] = DBNull.Value;
            nulls["long"] = DBNull.Value;
            nulls["decimal"] = DBNull.Value;
            nulls["double"] = DBNull.Value;
            nulls["float"] = DBNull.Value;
            nulls["datetimeoffset"] = DBNull.Value;
            nulls["datetime"] = DBNull.Value;
            nulls["timespan"] = DBNull.Value;
            dt.Rows.Add(nulls);

            return dt;
        }

        [TestMethod]
        public void TestDataRowExGetBool()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            bool b1 = values.GetBoolean("bool");
            Assert.AreEqual(true, b1);

            bool b2 = values.GetBoolean("bool", false);
            Assert.AreEqual(true, b2);

            bool b3 = nulls.GetBoolean("bool", false);
            Assert.AreEqual(false, b3);

            bool? b4 = values.GetNullableBoolean("bool");
            Assert.AreEqual(true, b4);

            bool? b5 = nulls.GetNullableBoolean("bool");
            Assert.AreEqual(null, b5);
        }

        [TestMethod]
        public void TestDataRowExGetByte()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            byte b1 = values.GetByte("byte");
            Assert.AreEqual(byte.MaxValue, b1);

            byte b2 = values.GetByte("byte", 0);
            Assert.AreEqual(byte.MaxValue, b2);

            byte b3 = nulls.GetByte("byte", 0);
            Assert.AreEqual(0, b3);

            byte? b4 = values.GetNullableByte("byte");
            Assert.AreEqual(byte.MaxValue, b4);

            byte? b5 = nulls.GetNullableByte("byte");
            Assert.AreEqual(null, b5);
        }

        [TestMethod]
        public void TestDataRowExGetInt16()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            short i1 = values.GetInt16("short");
            Assert.AreEqual(short.MaxValue, i1);

            short i2 = values.GetInt16("short", 0);
            Assert.AreEqual(short.MaxValue, i2);

            short i3 = nulls.GetInt16("short", 0);
            Assert.AreEqual(0, i3);

            short? i4 = values.GetNullableInt16("short");
            Assert.AreEqual(short.MaxValue, i4);

            short? i5 = nulls.GetNullableInt16("short");
            Assert.AreEqual(null, i5);
        }

        [TestMethod]
        public void TestDataRowExGetInt32()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            int i1 = values.GetInt32("int");
            Assert.AreEqual(int.MaxValue, i1);

            int i2 = values.GetInt32("int", 0);
            Assert.AreEqual(int.MaxValue, i2);

            int i3 = nulls.GetInt32("int", 0);
            Assert.AreEqual(0, i3);

            int? i4 = values.GetNullableInt32("int");
            Assert.AreEqual(int.MaxValue, i4);

            int? i5 = nulls.GetNullableInt32("int");
            Assert.AreEqual(null, i5);
        }

        [TestMethod]
        public void TestDataRowExGetInt64()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            long i1 = values.GetInt64("long");
            Assert.AreEqual(long.MaxValue, i1);

            long i2 = values.GetInt64("long", 0);
            Assert.AreEqual(long.MaxValue, i2);

            long i3 = nulls.GetInt64("long", 0);
            Assert.AreEqual(0, i3);

            long? i4 = values.GetNullableInt64("long");
            Assert.AreEqual(long.MaxValue, i4);

            long? i5 = nulls.GetNullableInt64("long");
            Assert.AreEqual(null, i5);
        }

        [TestMethod]
        public void TestDataRowExGetDecimal()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            decimal d1 = values.GetDecimal("decimal");
            Assert.AreEqual(decimal.MaxValue, d1);

            decimal d2 = values.GetDecimal("decimal", 0);
            Assert.AreEqual(decimal.MaxValue, d2);

            decimal d3 = nulls.GetDecimal("decimal", 0);
            Assert.AreEqual(0, d3);

            decimal? d4 = values.GetNullableDecimal("decimal");
            Assert.AreEqual(decimal.MaxValue, d4);

            decimal? d5 = nulls.GetNullableDecimal("decimal");
            Assert.AreEqual(null, d5);
        }

        [TestMethod]
        public void TestDataRowExGetDouble()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            double d1 = values.GetDouble("double");
            Assert.AreEqual(double.MaxValue, d1);

            double d2 = values.GetDouble("double", 0);
            Assert.AreEqual(double.MaxValue, d2);

            double d3 = nulls.GetDouble("double", 0);
            Assert.AreEqual(0, d3);

            double? d4 = values.GetNullableDouble("double");
            Assert.AreEqual(double.MaxValue, d4);

            double? d5 = nulls.GetNullableDouble("double");
            Assert.AreEqual(null, d5);
        }

        [TestMethod]
        public void TestDataRowExGetFloat()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            float f1 = values.GetSingle("float");
            Assert.AreEqual(float.MaxValue, f1);

            float f2 = values.GetSingle("float", 0);
            Assert.AreEqual(float.MaxValue, f2);

            float f3 = nulls.GetSingle("float", 0);
            Assert.AreEqual(0, f3);

            float? f4 = values.GetNullableSingle("float");
            Assert.AreEqual(float.MaxValue, f4);

            float? f5 = nulls.GetNullableSingle("float");
            Assert.AreEqual(null, f5);
        }

        [TestMethod]
        public void TestDataRowExGetDateTimeOffset()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            DateTimeOffset d1 = values.GetDateTimeOffset("datetimeoffset");
            Assert.AreEqual(DateTimeOffset.MaxValue, d1);

            DateTimeOffset d2 = values.GetDateTimeOffset("datetimeoffset", DateTimeOffset.MinValue);
            Assert.AreEqual(DateTimeOffset.MaxValue, d2);

            DateTimeOffset d3 = nulls.GetDateTimeOffset("datetimeoffset", DateTimeOffset.MinValue);
            Assert.AreEqual(DateTimeOffset.MinValue, d3);

            DateTimeOffset? d4 = values.GetNullableDateTimeOffset("datetimeoffset");
            Assert.AreEqual(DateTimeOffset.MaxValue, d4);

            DateTimeOffset? d5 = nulls.GetNullableDateTimeOffset("datetimeoffset");
            Assert.AreEqual(null, d5);
        }

        [TestMethod]
        public void TestDataRowExGetDateTime()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            DateTime d1 = values.GetDateTime("datetime");
            Assert.AreEqual(DateTime.MaxValue, d1);

            DateTime d2 = values.GetDateTime("datetime", DateTime.MinValue);
            Assert.AreEqual(DateTime.MaxValue, d2);

            DateTime d3 = nulls.GetDateTime("datetime", DateTime.MinValue);
            Assert.AreEqual(DateTime.MinValue, d3);

            DateTime? d4 = values.GetNullableDateTime("datetime");
            Assert.AreEqual(DateTime.MaxValue, d4);

            DateTime? d5 = nulls.GetNullableDateTime("datetime");
            Assert.AreEqual(null, d5);
        }

        [TestMethod]
        public void TestDataRowExGetTimeSpan()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            TimeSpan t1 = values.GetTimeSpan("timespan");
            Assert.AreEqual(TimeSpan.MaxValue, t1);

            TimeSpan t2 = values.GetTimeSpan("timespan", TimeSpan.MinValue);
            Assert.AreEqual(TimeSpan.MaxValue, t2);

            TimeSpan t3 = nulls.GetTimeSpan("timespan", TimeSpan.MinValue);
            Assert.AreEqual(TimeSpan.MinValue, t3);

            TimeSpan? t4 = values.GetNullableTimeSpan("timespan");
            Assert.AreEqual(TimeSpan.MaxValue, t4);

            TimeSpan? t5 = nulls.GetNullableTimeSpan("timespan");
            Assert.AreEqual(null, t5);
        }

        [TestMethod]
        public void TestDataRowExGetString()
        {
            DataTable dt = makeTestTable();
            DataRow values = dt.Rows[0];
            DataRow nulls = dt.Rows[1];

            string s1 = values.GetString("string");
            Assert.AreEqual("Hello", s1);

            string s2 = values.GetString("string", String.Empty);
            Assert.AreEqual("Hello", s2);

            string s3 = nulls.GetString("string", String.Empty);
            Assert.AreEqual(String.Empty, s3);
        }
    }
}
