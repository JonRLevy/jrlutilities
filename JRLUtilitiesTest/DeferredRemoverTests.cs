﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using JonRLevy.JRLUtilities.Collections.Generic;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class DeferredRemoverTests
    {
        [TestMethod]
        public void Test2Ints1Removed()
        {
            List<int> TestList = new List<int>();
            TestList.Add(1);
            TestList.Add(2);

            DeferredRemover<int> dr = new DeferredRemover<int>(TestList);
            dr.RemoveDeferred(TestList[0]);

            int RemoveResult = dr.Remove();

            Assert.AreEqual(0, RemoveResult, "Unexpected return from Remove()");
            Assert.AreEqual(1, TestList.Count, "Unexpected number of items in TestList after Remove()");
            Assert.AreEqual(2, TestList[0], "Unexpected value of first item in TestList after Remove()");
        }

        [TestMethod]
        public void Test2Objects1Removed()
        {
            List<Object> TestList = new List<Object>();
            Object obj1 = new object();
            Object obj2 = new object();
            TestList.Add(obj1);
            TestList.Add(obj2);

            DeferredRemover<Object> dr = new DeferredRemover<Object>(TestList);
            dr.RemoveDeferred(obj1);
            dr.Remove();

            Assert.AreEqual(1, TestList.Count, "Unexpected number of items in TestList after Remove()");
            Assert.AreEqual(obj2, TestList[0], "Unexpected value of first item in TestList after Remove()");
        }

        [TestMethod]
        public void Test2ObjectsRemoveNonExisting()
        {
            List<Object> TestList = new List<Object>();
            Object obj1 = new object();
            Object obj2 = new object();
            Object obj3 = new object();
            TestList.Add(obj1);
            TestList.Add(obj2);

            DeferredRemover<Object> dr = new DeferredRemover<Object>(TestList);
            
            dr.RemoveDeferred(obj3);


            int RemoveResult = dr.Remove();

            Assert.AreEqual(1, RemoveResult, "Unexpected return from Remove()");
            Assert.AreEqual(2, TestList.Count, "Unexpected number of items in TestList after Remove()");
            Assert.AreEqual(obj1, TestList[0], "Unexpected value of first item in TestList after Remove()");
        }

        [TestMethod]
        public void NullCollection()
        {
            Object obj1 = new object();

            DeferredRemover<Object> dr = new DeferredRemover<Object>(null);

            dr.RemoveDeferred(obj1);

            int RemoveResult = dr.Remove();

            Assert.AreEqual(-1, RemoveResult, "Unexpected return from Remove()");
        }

        [TestMethod]
        public void Test2ObjectsRemoveNullObject()
        {
            List<Object> TestList = new List<Object>();
            Object obj1 = new object();
            Object obj2 = new object();
            TestList.Add(obj1);
            TestList.Add(obj2);

            DeferredRemover<Object> dr = new DeferredRemover<Object>(TestList);

            dr.RemoveDeferred(null);


            int RemoveResult = dr.Remove();

            Assert.AreEqual(1, RemoveResult, "Unexpected return from Remove()");
            Assert.AreEqual(2, TestList.Count, "Unexpected number of items in TestList after Remove()");
            Assert.AreEqual(obj1, TestList[0], "Unexpected value of first item in TestList after Remove()");
        }

        [TestMethod]
        public void TestRemoveRange()
        {
            List<int> TestList = new List<int>();
            TestList.Add(1);
            TestList.Add(2);
            TestList.Add(3);
            TestList.Add(4);

            DeferredRemover<int> dr = new DeferredRemover<int>(TestList);
            dr.RemoveDeferredRange(TestList.Where(i => i % 2 == 1));
            dr.Remove();

            Assert.AreEqual(2, TestList.Count, "Unexpected number of items in TestList after Remove()");
            Assert.AreEqual(2, TestList[0], "Unexpected value of first item in TestList after Remove()");
        }

        [TestMethod]
        public void TestAddViaRemoveDeferred()
        {
            List<int> OddList = new List<int>();
            OddList.Add(1);
            OddList.Add(2);
            OddList.Add(3);
            OddList.Add(4);
            OddList.Add(5);

            List<int> EvenList = new List<int>();

            DeferredRemover<int> dr = new DeferredRemover<int>(OddList);

            foreach (int number in OddList)
            {
                if (number % 2 == 0)
                EvenList.Add(dr.RemoveDeferred(number));
            }

            dr.Remove();
            Assert.AreEqual(3, OddList.Count, "Unexpected number of items in OddList after Remove()");
            Assert.AreEqual(2, EvenList.Count, "Unexpected number of items in EvenList");
            Assert.AreEqual(1, OddList[0], "Unexpected value of first item in OddList after Remove()");
            Assert.AreEqual(2, EvenList[0], "Unexpected value of first item in EvenList");
        }
    }
}
