﻿using JonRLevy.JRLUtilities.Collections;
using JonRLevy.JRLUtilities.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class IEnumerableExTests
    {
        [TestMethod]
        public void IEnumarableExIntEvery3Test()
        {
            List<int> testList = new List<int>();
            for (int c=0; c<5; c++)
            {
                testList.Add(c);
            }

            List<int> resultList = testList.EveryNth(3).ToList();
            Assert.AreEqual(2, resultList.Count);
            Assert.AreEqual(0, resultList[0]);
            Assert.AreEqual(3, resultList[1]);
        }

        [TestMethod]
        public void IEnumarableExIntEvery0Test()
        {
            List<int> testList = new List<int>();
            for (int c = 0; c < 5; c++)
            {
                testList.Add(c);
            }

            List<int> resultList = testList.EveryNth(0).ToList();
            Assert.AreEqual(0, resultList.Count);
        }

        [TestMethod]
        public void IEnumarableExEvery3Test()
        {
            ArrayList testList = new ArrayList();
            Object obj1 = new object();
            Object obj2 = new object();
            Object obj3 = new object();
            Object obj4 = new object();
            Object obj5 = new object();

            testList.Add(obj1);
            testList.Add(obj2);
            testList.Add(obj3);
            testList.Add(obj4);
            testList.Add(obj5);
            ArrayList resultList = new ArrayList();
            foreach (Object resultObject in testList.EveryNth(3))
                resultList.Add(resultObject);

            Assert.AreEqual(2, resultList.Count);
            Assert.AreSame(obj1, resultList[0]);
            Assert.AreEqual(obj4, resultList[1]);
        }

        [TestMethod]
        public void IEnumarableExEvery0Test()
        {
            ArrayList testList = new ArrayList();
            Object obj1 = new object();
            Object obj2 = new object();
            Object obj3 = new object();
            Object obj4 = new object();
            Object obj5 = new object();

            testList.Add(obj1);
            testList.Add(obj2);
            testList.Add(obj3);
            testList.Add(obj4);
            testList.Add(obj5);
            ArrayList resultList = new ArrayList();
            foreach (Object resultObject in testList.EveryNth(0))
                resultList.Add(resultObject);

            Assert.AreEqual(0, resultList.Count);
        }

        [TestMethod]
        public void IEnumarableExIntSampleTest()
        {
            RandomSampler<int> testSampler = new RandomSampler<int>(0, 0.5);
            List<int> testList = new List<int>();
            for (int c = 0; c < 6; c++)
            {
                testList.Add(c);
            }

            List<int> resultList = testList.Sample(testSampler).ToList();
            Assert.AreEqual(1, resultList.Count);
            Assert.AreEqual(4, resultList[0]);
        }

        [TestMethod]
        public void IEnumerableExPermutation0ItemsTest()
        {
            List<int> t = new List<int>();

            int count = 0;

            foreach (IEnumerable<int> permutation in t.Permutate())
                count++;

            Assert.AreEqual(0, count);
        }

        [TestMethod]
        public void IEnumerableExPermutation1ItemTest()
        {
            List<int> t = new List<int>();
            t.Add(1);

            int count = 0;

            foreach (IEnumerable<int> permutation in t.Permutate())
            {
                count++;

                if (count == 1)
                {
                    int idx = 0;
                    foreach (int i in permutation)
                    {
                        idx++;
                        if (idx == 1)
                            Assert.AreEqual(1, i);
                        else if (idx > 1)
                            Assert.Fail($"Wrong idx: {idx}");
                    }
                }
                else if (count > 1)
                    Assert.Fail($"Wrong count: {count}");
            }

            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public void IEnumerableExPermutation2ItemsTest()
        {
            List<int> t = new List<int>();
            t.Add(1);
            t.Add(2);

            int count = 0;

            foreach (IEnumerable<int> permutation in t.Permutate())
            {
                count++;

                if (count == 1)
                {
                    int idx = 0;
                    foreach (int i in permutation)
                    {
                        idx++;
                        if (idx == 1)
                            Assert.AreEqual(1, i);
                        else if (idx == 2)
                            Assert.AreEqual(2, i);
                        else if (idx > 2)
                            Assert.Fail($"Wrong idx: {idx}");
                    }
                }
                else if (count == 2)
                {
                    int idx = 0;
                    foreach (int i in permutation)
                    {
                        idx++;
                        if (idx == 1)
                            Assert.AreEqual(2, i);
                        else if (idx == 2)
                            Assert.AreEqual(1, i);
                        else if (idx > 2)
                            Assert.Fail($"Wrong idx: {idx}");
                    }
                }
                else if (count > 2)
                    Assert.Fail($"Wrong count: {count}");
            }

            Assert.AreEqual(2, count);
        }

        [TestMethod]
        public void IEnumerableExPermutation3ItemsTest()
        {
            List<char> t = new List<char>();
            t.Add('a');
            t.Add('b');
            t.Add('c');

            int count = 0;

            foreach (IEnumerable<char> permutation in t.Permutate())
            {
                count++;

                if (count == 1)
                {
                    int idx = 0;
                    foreach (int i in permutation)
                    {
                        idx++;
                        if (idx == 1)
                            Assert.AreEqual('a', i);
                        else if (idx == 2)
                            Assert.AreEqual('b', i);
                        else if (idx == 3)
                            Assert.AreEqual('c', i);
                        else if (idx > 3)
                            Assert.Fail($"Wrong idx: {idx}");
                    }
                }
                else if (count == 2)
                {
                    int idx = 0;
                    foreach (int i in permutation)
                    {
                        idx++;
                        if (idx == 1)
                            Assert.AreEqual('a', i);
                        else if (idx == 2)
                            Assert.AreEqual('c', i);
                        else if (idx == 3)
                            Assert.AreEqual('b', i);
                        else if (idx > 3)
                            Assert.Fail($"Wrong idx: {idx}");
                    }
                }
                else if (count == 3)
                {
                    int idx = 0;
                    foreach (int i in permutation)
                    {
                        idx++;
                        if (idx == 1)
                            Assert.AreEqual('b', i);
                        else if (idx == 2)
                            Assert.AreEqual('a', i);
                        else if (idx == 3)
                            Assert.AreEqual('c', i);
                        else if (idx > 3)
                            Assert.Fail($"Wrong idx: {idx}");
                    }
                }
                else if (count == 4)
                {
                    int idx = 0;
                    foreach (int i in permutation)
                    {
                        idx++;
                        if (idx == 1)
                            Assert.AreEqual('b', i);
                        else if (idx == 2)
                            Assert.AreEqual('c', i);
                        else if (idx == 3)
                            Assert.AreEqual('a', i);
                        else if (idx > 3)
                            Assert.Fail($"Wrong idx: {idx}");
                    }
                }
                else if (count == 5)
                {
                    int idx = 0;
                    foreach (int i in permutation)
                    {
                        idx++;
                        if (idx == 1)
                            Assert.AreEqual('c', i);
                        else if (idx == 2)
                            Assert.AreEqual('a', i);
                        else if (idx == 3)
                            Assert.AreEqual('b', i);
                        else if (idx > 3)
                            Assert.Fail($"Wrong idx: {idx}");
                    }
                }
                else if (count == 6)
                {
                    int idx = 0;
                    foreach (int i in permutation)
                    {
                        idx++;
                        if (idx == 1)
                            Assert.AreEqual('c', i);
                        else if (idx == 2)
                            Assert.AreEqual('b', i);
                        else if (idx == 3)
                            Assert.AreEqual('a', i);
                        else if (idx > 3)
                            Assert.Fail($"Wrong idx: {idx}");
                    }
                }
                else if (count > 6)
                    Assert.Fail($"Wrong count: {count}");
            }

            Assert.AreEqual(6, count);
        }
    }
}
