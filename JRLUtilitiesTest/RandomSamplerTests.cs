﻿using JonRLevy.JRLUtilities.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class RandomSamplerTests
    {
        [TestMethod]
        public void RandomSamplerSamplingTest()
        {
            RandomSampler<int> testSampler = new RandomSampler<int>(0, 0.5);

            bool result1 = testSampler.Keep(0);
            Assert.AreEqual(false, result1, "result1 is wrong");
            bool result2 = testSampler.Keep(0);
            Assert.AreEqual(false, result2, "result2 is wrong");
            bool result3 = testSampler.Keep(0);
            Assert.AreEqual(false, result3, "result3 is wrong");
            bool result4 = testSampler.Keep(0);
            Assert.AreEqual(false, result4, "result4 is wrong");
            bool result5 = testSampler.Keep(0);
            Assert.AreEqual(true, result5, "result5 is wrong");
            bool result6 = testSampler.Keep(0);
            Assert.AreEqual(false, result6, "result6 is wrong");
        }

        [TestMethod]
        public void RandomSamplerIEnumerableSampleTest()
        {
            RandomSampler<int> testSampler = new RandomSampler<int>(0, 0.5);
            List<int> testList = new List<int>();
            for (int c = 0; c < 6; c++)
            {
                testList.Add(c);
            }

            List<int> resultList = testSampler.Sample(testList).ToList();
            Assert.AreEqual(1, resultList.Count);
            Assert.AreEqual(4, resultList[0]);
        }
    }
}
