﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JonRLevy.JRLUtilities.Database;
using System.Data.SqlClient;

namespace JRLUtilitiesTest
{
    [TestClass]
    public class SqlParameterCollectionExTests
    {
        [TestMethod]
        public void TestSqlParameterCollectionExAddBit()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddBit("testname1", true);
            Assert.AreEqual(1, pc.Count, "Unexpected return from AddBit");
            Assert.AreSame(p1, pc[0], "Unexpected return from AddBit");
            Assert.AreEqual(p1.Value, true, "Unexpected return from AddBit");

            SqlParameter p2 = pc.AddBit("testname2", null);
            Assert.AreEqual(2, pc.Count, "Unexpected return from AddBit");
            Assert.AreSame(p2, pc[1], "Unexpected return from AddBit");
            Assert.AreEqual(p2.Value, DBNull.Value, "Unexpected return from AddBit");
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddTinyInt()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddTinyInt("testname1", Byte.MaxValue);
            Assert.AreEqual(1, pc.Count, "Unexpected return from AddTinyInt");
            Assert.AreSame(p1, pc[0], "Unexpected return from AddTinyInt");
            Assert.AreEqual(p1.Value, Byte.MaxValue, "Unexpected return from AddTinyInt");

            SqlParameter p2 = pc.AddTinyInt("testname2", null);
            Assert.AreEqual(2, pc.Count, "Unexpected return from AddTinyInt");
            Assert.AreSame(p2, pc[1], "Unexpected return from AddTinyInt");
            Assert.AreEqual(p2.Value, DBNull.Value, "Unexpected return from AddTinyInt");
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddSmallInt()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddSmallInt("testname1", short.MaxValue);
            Assert.AreEqual(1, pc.Count, "Unexpected return from AddSmallInt");
            Assert.AreSame(p1, pc[0], "Unexpected return from AddSmallInt");
            Assert.AreEqual(p1.Value, short.MaxValue, "Unexpected return from AddSmallInt");

            SqlParameter p2 = pc.AddSmallInt("testname2", null);
            Assert.AreEqual(2, pc.Count, "Unexpected return from AddSmallInt");
            Assert.AreSame(p2, pc[1], "Unexpected return from AddSmallInt");
            Assert.AreEqual(p2.Value, DBNull.Value, "Unexpected return from AddSmallInt");
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddInt()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1=pc.AddInt("testname1", int.MaxValue);
            Assert.AreEqual(1, pc.Count, "Unexpected return from AddInt");
            Assert.AreSame(p1, pc[0], "Unexpected return from AddInt");
            Assert.AreEqual(p1.Value, int.MaxValue, "Unexpected return from AddInt");

            SqlParameter p2 = pc.AddInt("testname2", null);
            Assert.AreEqual(2, pc.Count, "Unexpected return from AddInt");
            Assert.AreSame(p2, pc[1], "Unexpected return from AddInt");
            Assert.AreEqual(p2.Value, DBNull.Value, "Unexpected return from AddInt");
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddBigInt()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddBigInt("testname1", long.MaxValue);
            Assert.AreEqual(1, pc.Count, "Unexpected return from AddBigInt");
            Assert.AreSame(p1, pc[0], "Unexpected return from AddBigInt");
            Assert.AreEqual(p1.Value, long.MaxValue, "Unexpected return from AddBigInt");

            SqlParameter p2 = pc.AddBigInt("testname2", null);
            Assert.AreEqual(2, pc.Count, "Unexpected return from AddBigInt");
            Assert.AreSame(p2, pc[1], "Unexpected return from AddBigInt");
            Assert.AreEqual(p2.Value, DBNull.Value, "Unexpected return from AddBigInt");
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddDecimal()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddDecimal("testname1", decimal.MaxValue);
            Assert.AreEqual(1, pc.Count, "Unexpected return from AddDecimal");
            Assert.AreSame(p1, pc[0], "Unexpected return from AddDecimal");
            Assert.AreEqual(p1.Value, decimal.MaxValue, "Unexpected return from AddDecimal");

            SqlParameter p2 = pc.AddDecimal("testname2", null);
            Assert.AreEqual(2, pc.Count, "Unexpected return from AddDecimal");
            Assert.AreSame(p2, pc[1], "Unexpected return from AddDecimal");
            Assert.AreEqual(p2.Value, DBNull.Value, "Unexpected return from AddDecimal");
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddFloat()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddFloat("testname1", double.MaxValue);
            Assert.AreEqual(1, pc.Count, "Unexpected return from AddFloat");
            Assert.AreSame(p1, pc[0], "Unexpected return from AddFloat");
            Assert.AreEqual(p1.Value, double.MaxValue, "Unexpected return from AddFloat");

            SqlParameter p2 = pc.AddFloat("testname2", null);
            Assert.AreEqual(2, pc.Count, "Unexpected return from AddFloat");
            Assert.AreSame(p2, pc[1], "Unexpected return from AddFloat");
            Assert.AreEqual(p2.Value, DBNull.Value, "Unexpected return from AddFloat");
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddReal()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddReal("testname1", float.MaxValue);
            Assert.AreEqual(1, pc.Count, "Unexpected return from AddReal");
            Assert.AreSame(p1, pc[0], "Unexpected return from AddReal");
            Assert.AreEqual(p1.Value, float.MaxValue, "Unexpected return from AddReal");

            SqlParameter p2 = pc.AddReal("testname2", null);
            Assert.AreEqual(2, pc.Count, "Unexpected return from AddReal");
            Assert.AreSame(p2, pc[1], "Unexpected return from AddReal");
            Assert.AreEqual(p2.Value, DBNull.Value, "Unexpected return from AddReal");
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddDateTimeOffset()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddDateTimeOffset("testname1", DateTimeOffset.MaxValue);
            Assert.AreEqual(1, pc.Count);
            Assert.AreSame(p1, pc[0]);
            Assert.AreEqual(p1.Value, DateTimeOffset.MaxValue);

            SqlParameter p2 = pc.AddDateTimeOffset("testname2", null);
            Assert.AreEqual(2, pc.Count);
            Assert.AreSame(p2, pc[1]);
            Assert.AreEqual(p2.Value, DBNull.Value);
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddDateTime2()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddDateTime2("testname1", DateTime.MaxValue);
            Assert.AreEqual(1, pc.Count);
            Assert.AreSame(p1, pc[0]);
            Assert.AreEqual(p1.Value, DateTime.MaxValue);

            SqlParameter p2 = pc.AddDateTime2("testname2", null);
            Assert.AreEqual(2, pc.Count);
            Assert.AreSame(p2, pc[1]);
            Assert.AreEqual(p2.Value, DBNull.Value);
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddDateTime()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddDateTime("testname1", DateTime.MaxValue);
            Assert.AreEqual(1, pc.Count);
            Assert.AreSame(p1, pc[0]);
            Assert.AreEqual(p1.Value, DateTime.MaxValue);

            SqlParameter p2 = pc.AddDateTime("testname2", null);
            Assert.AreEqual(2, pc.Count);
            Assert.AreSame(p2, pc[1]);
            Assert.AreEqual(p2.Value, DBNull.Value);
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddSmallDateTime()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            SqlParameter p1 = pc.AddSmallDateTime("testname1", DateTime.MaxValue);
            Assert.AreEqual(1, pc.Count);
            Assert.AreSame(p1, pc[0]);
            Assert.AreEqual(p1.Value, DateTime.MaxValue);

            SqlParameter p2 = pc.AddSmallDateTime("testname2", null);
            Assert.AreEqual(2, pc.Count);
            Assert.AreSame(p2, pc[1]);
            Assert.AreEqual(p2.Value, DBNull.Value);
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddTime()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            TimeSpan tsTest = new TimeSpan(1, 1, 1);
            SqlParameter p1 = pc.AddTime("testname1", tsTest);
            Assert.AreEqual(1, pc.Count);
            Assert.AreSame(p1, pc[0]);
            Assert.AreEqual(p1.Value, tsTest);

            SqlParameter p2 = pc.AddTime("testname2", null);
            Assert.AreEqual(2, pc.Count);
            Assert.AreSame(p2, pc[1]);
            Assert.AreEqual(p2.Value, DBNull.Value);
        }

        [TestMethod]
        public void TestSqlParameterCollectionExAddNVarChar()
        {
            SqlCommand com = new SqlCommand();
            SqlParameterCollection pc = com.Parameters;

            string sTest = "Hello!";

            SqlParameter p1 = pc.AddNVarChar("testname1", sTest);
            Assert.AreEqual(1, pc.Count);
            Assert.AreSame(p1, pc[0]);
            Assert.AreEqual(p1.Value, sTest);

            SqlParameter p2 = pc.AddNVarChar("testname2", null);
            Assert.AreEqual(2, pc.Count);
            Assert.AreSame(p2, pc[1]);
            Assert.AreEqual(p2.Value, DBNull.Value);
        }
    }
}
